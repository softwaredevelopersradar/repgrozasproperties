﻿using DllGrozaSProperties.Models;
using DllGrozaSProperties.Models.Global;
using DllGrozaSProperties.Models.Local;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Xml;
using WpfPasswordControlLibrary;
using CoordFormatLib;

namespace DllGrozaSProperties
{
    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class ControlProperties : UserControl, INotifyPropertyChanged
    {
        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion

        #region Events
        public event EventHandler<LocalProperties> OnLocalPropertiesChanged = (sender, newModel) => { };
        public event EventHandler<GlobalProperties> OnGlobalPropertiesChanged = (sender, newModel) => { };
        public event EventHandler OnLocalDefaultButtonClick = (sender, obj) => { };
        public event EventHandler OnGlobalDefaultButtonClick = (sender, obj) => { };
        public event EventHandler<Languages> OnLanguageChanged = (sender, language) => { };
        public event EventHandler<bool> OnPasswordChecked = (sender, isCorrect) => { };
        #endregion

        #region Properties

        private readonly WindowPass _accessWindowPass = new WindowPass();

        private bool isLocalSelected;
        public bool IsLocalSelected
        {
            get => isLocalSelected;
            set
            {
                if (isLocalSelected == value) return;
                isLocalSelected = value;
                OnPropertyChanged();
            }
        }

        private bool isValid;
        public bool IsValid
        {
            get => isValid;
            private set
            {
                if (isValid == value) return;
                isValid = value;
                OnPropertyChanged();
            }
        }

        private string errorMessage;
        public string ErrorMessage
        {
            get => errorMessage;
            set
            {
                if (errorMessage == value) return;
                errorMessage = value;
                OnPropertyChanged();
            }
        }

        private bool _showToolTip;
        public bool ShowToolTip
        {
            get => _showToolTip;
            set
            {
                if (_showToolTip == value) return;
                _showToolTip = value;
                OnPropertyChanged();
            }
        }

        private LocalProperties savedLocal;
        private GlobalProperties savedGlobal;

        public LocalProperties Local
        {
            get => (LocalProperties)Resources["localProperties"];
            set
            {
                if ((LocalProperties)Resources["localProperties"] == value)
                {
                    savedLocal = value.Clone();
                    return;
                }
                ((LocalProperties)Resources["localProperties"]).Update(value);
                savedLocal = value.Clone();
            }
        }

        public GlobalProperties Global
        {
            get => (GlobalProperties)Resources["globalProperties"];
            set
            {
                if ((GlobalProperties)Resources["globalProperties"] == value)
                {
                    savedGlobal = value.Clone();
                    return;
                }
                ((GlobalProperties)Resources["globalProperties"]).Update(value);
                savedGlobal = value.Clone();
            }
        }

        #endregion

        #region Language
        Dictionary<string, string> TranslateDic;

        private void ChangeLanguage(Languages newLanguage)
        {
            SetDynamicResources(newLanguage);
            LoadDictionary();
            SetCategoryGlobalNames();
            SetCategoryLocalNames();
        }
        private void SetDynamicResources(Languages newLanguage)
        {
            try
            {
                ResourceDictionary dict = new ResourceDictionary();
                switch (newLanguage)
                {
                    case Languages.EN:
                        dict.Source = new Uri("/DllGrozaSProperties;component/Languages/StringResource.EN.xaml",
                                      UriKind.Relative);
                        break;
                    case Languages.RU:
                        dict.Source = new Uri("/DllGrozaSProperties;component/Languages/StringResource.RU.xaml",
                                           UriKind.Relative);
                        break;
                    case Languages.AZ:
                        dict.Source = new Uri("/DllGrozaSProperties;component/Languages/StringResource.AZ.xaml",
                                           UriKind.Relative);
                        break;
                    case Languages.SR:
                        dict.Source = new Uri("/DllGrozaSProperties;component/Languages/StringResource.SR.xaml",
                            UriKind.Relative);
                        break;
                    default:
                        dict.Source = new Uri("/DllGrozaSProperties;component/Languages/StringResource.EN.xaml",
                                          UriKind.Relative);
                        break;
                }
                Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception)
            {
                //TODO
            }
        }

        void LoadDictionary()
        {
            var translation = Properties.Resources.Translation;
            XmlDocument xDoc = new XmlDocument();
            xDoc.LoadXml(translation);
            TranslateDic = new Dictionary<string, string>();
            // получим корневой элемент
            XmlElement xRoot = xDoc.DocumentElement;
            foreach (XmlNode x2Node in xRoot.ChildNodes)
            {
                if (x2Node.NodeType == XmlNodeType.Comment)
                    continue;

                // получаем атрибут ID
                if (x2Node.Attributes.Count > 0)
                {
                    XmlNode attr = x2Node.Attributes.GetNamedItem("ID");
                    if (attr != null)
                    {
                        foreach (XmlNode childnode in x2Node.ChildNodes)
                        {
                            // если узел - language
                            if (childnode.Name == Local.General.Language.ToString())
                            {
                                if (!TranslateDic.ContainsKey(attr.Value))
                                    TranslateDic.Add(attr.Value, childnode.InnerText);
                            }
                        }
                    }
                }
            }
        }

        private void SetCategoryGlobalNames()
        {
            try
            {
                foreach (var nameCategory in PropertyGlobal.Categories.Select(category => category.Name).ToList())
                {
                    var prop = PropertyGlobal.Categories.FirstOrDefault(t => t.Name == nameCategory);
                    if (prop != null && TranslateDic.ContainsKey(nameCategory))
                        prop.HeaderCategoryName = TranslateDic[nameCategory];
                }
            }
            catch (Exception)
            {

            }
        }

        private void SetCategoryLocalNames()
        {
            try
            {
                foreach (var nameCategory in PropertyLocal.Categories.Select(category => category.Name).ToList())
                {
                     PropertyLocal.Categories.First(t => t.Name == nameCategory).HeaderCategoryName = TranslateDic[nameCategory];
                }
            }
            catch (Exception)
            {

            }
        }
        #endregion

        #region Validation

        private void Validate()
        {
            bool isvalid = true;
            string error = "";
            Action<object> funcValidation = (object obj) =>
            {
                foreach (var property in obj.GetType().GetProperties())
                {
                    foreach (var subProperty in property.PropertyType.GetProperties())
                    {
                        try
                        {
                            if (!(property.Name == "DependencyObjectType" || property.Name == "Dispatcher"))
                            {
                                    if (!string.IsNullOrWhiteSpace(Convert.ToString(((IDataErrorInfo)property.GetValue(obj))[subProperty.Name])))
                                    {
                                        error += $"{TranslateDic[property.Name]}: {TranslateDic[subProperty.Name]}" + "\n";
                                        isvalid = false;
                                    }                               
                            }
                        }
                        catch
                        { 
                            //  Console.WriteLine(property.Name+ "  " + subProperty.Name);
                            continue;
                        }
                       
                    }
                }
                IsValid = isvalid;
                ShowToolTip = !isvalid;
                ErrorMessage = error;
                return;
            };

            if (IsLocalSelected)
            {
                funcValidation(Local);
            }
            else
            {
                funcValidation(Global);
            }
        }

        #endregion

        #region Access
        private bool _isAccessChecked = false;

        private void InitAccessWindow()
        {
            _accessWindowPass.OnEnterInvalidPassword += _accessWindowPass_OnEnterInvalidPassword;
            _accessWindowPass.OnEnterValidPassword += _accessWindowPass_OnEnterValidPassword;
            _accessWindowPass.OnClosePasswordBox += _accessWindowPass_OnClosePasswordBox;
            _accessWindowPass.Resources = Resources;
            _accessWindowPass.SetResourceReference(WindowPass.LablePasswordProperty, "labelPassword");
        }

        private void _accessWindowPass_OnEnterValidPassword(object sender, EventArgs e)
        {
            _isAccessChecked = true;
            Local.General.Access = AccessTypes.Admin;
            ChangeVisibilityLocalProperties(true);
            ChangeVisibilityGlobalProperties(true);
            _accessWindowPass.Hide();
            OnPasswordChecked(this, true);
        }

        private void _accessWindowPass_OnClosePasswordBox(object sender, EventArgs e)
        {
            Local.General.Access = AccessTypes.User;
        }

        private void _accessWindowPass_OnEnterInvalidPassword(object sender, EventArgs e)
        {
            Local.General.Access = AccessTypes.User;
            _accessWindowPass.Hide();

            OnPasswordChecked(this, false);
        }

        private void CheckLocalAccess()
        {
            if (!_isAccessChecked)
            {
                Task.Run(() =>
                 Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                   {
                       Local.General.Access = AccessTypes.User;
                       _accessWindowPass.ShowDialog();
                   }));
            }
            else
                _isAccessChecked = false;
        }

        public void SetPassword(string password)
        {
            _accessWindowPass.ValidPassword = password;
        }

        #endregion

        #region Local Global methods

        private void InitGlobalProperties()
        {
            SetBindingAccess();
            savedGlobal = Global.Clone();
            Global.OnPropertyChanged += OnPropertyChanged;
          

            PropertyGlobal.Editors.Add(new Editors.EditorCustom(nameof(GlobalProperties.RadioIntelegence), typeof(GlobalProperties), typeof(RadioIntelegence)));
            PropertyGlobal.Editors.Add(new Editors.EditorCustom(nameof(GlobalProperties.Jamming), typeof(GlobalProperties), typeof(Jamming)));
            PropertyGlobal.Editors.Add(new Editors.EditorCustom(nameof(GlobalProperties.GNSS), typeof(GlobalProperties), "GNSSGlobalEditorKey"));
            PropertyGlobal.Editors.Add(new Editors.EditorCustom(nameof(GlobalProperties.Spoofing), typeof(GlobalProperties), "SpoofingEditorKey"));
            PropertyGlobal.Editors.Add(new Editors.EditorCustom(nameof(GlobalProperties.CmpRX), typeof(GlobalProperties), typeof(CmpGlobal)));
            PropertyGlobal.Editors.Add(new Editors.EditorCustom(nameof(GlobalProperties.CmpTX), typeof(GlobalProperties), typeof(CmpGlobal)));
            PropertyGlobal.Editors.Add(new Editors.EditorCustom(nameof(GlobalProperties.EOM), typeof(GlobalProperties), typeof(OemGlobal)));
        }

        private void InitLocalProperties()
        {
            savedLocal = Local.Clone();
            savedLocal.General.PropertyChanged += SavedLocal_PropertyChanged;
            Local.General.PropertyChanged += Local_PropertyChanged;
            Local.OnPropertyChanged += OnPropertyChanged;

            PropertyLocal.Editors.Add(new Editors.EditorCustom(nameof(LocalProperties.General), typeof(LocalProperties), typeof(CommonLocal)));
            PropertyLocal.Editors.Add(new Editors.EditorCustom(nameof(LocalProperties.DB), typeof(LocalProperties), typeof(EndPointConnection)));
            PropertyLocal.Editors.Add(new Editors.EditorCustom(nameof(LocalProperties.DF), typeof(LocalProperties), typeof(DF)));
            PropertyLocal.Editors.Add(new Editors.EditorCustom(nameof(LocalProperties.RS), typeof(LocalProperties), typeof(RS)));
            PropertyLocal.Editors.Add(new Editors.EditorCustom(nameof(LocalProperties.SS), typeof(LocalProperties), typeof(SS)));
            PropertyLocal.Editors.Add(new Editors.EditorCustom(nameof(LocalProperties.ED), typeof(LocalProperties), typeof(EDConnection)));
            PropertyLocal.Editors.Add(new Editors.EditorCustom(nameof(LocalProperties.LC), typeof(LocalProperties), typeof(LC)));
            PropertyLocal.Editors.Add(new Editors.EditorCustom(nameof(LocalProperties.EOM), typeof(LocalProperties), typeof(UdpConnection)));
            PropertyLocal.Editors.Add(new Editors.EditorCustom(nameof(LocalProperties.Rodnik), typeof(LocalProperties), typeof(Rodnik)));
            PropertyLocal.Editors.Add(new Editors.EditorCustom(nameof(LocalProperties.Globus), typeof(LocalProperties), typeof(Globus)));
            PropertyLocal.Editors.Add(new Editors.EditorCustom(nameof(LocalProperties.Lemt), typeof(LocalProperties), typeof(Lemt)));

            PropertyLocal.Editors.Add(new Editors.EditorCustom(nameof(LocalProperties.Map), typeof(LocalProperties), typeof(Map)));
            PropertyLocal.Editors.Add(new Editors.EditorCustom(nameof(LocalProperties.CmpTX), typeof(LocalProperties), typeof(Cmp)));
            PropertyLocal.Editors.Add(new Editors.EditorCustom(nameof(LocalProperties.CmpRX), typeof(LocalProperties), typeof(Cmp)));
            PropertyLocal.Editors.Add(new Editors.EditorCustom(nameof(LocalProperties.Cuirasse_M), typeof(LocalProperties), typeof(CuirasseM)));
            PropertyLocal.Editors.Add(new Editors.EditorCustom(nameof(LocalProperties.Aeroscope), typeof(LocalProperties), typeof(Aeroscope)));
            PropertyLocal.Editors.Add(new Editors.EditorCustom(nameof(LocalProperties.Amp1), typeof(LocalProperties), typeof(Amp)));
            PropertyLocal.Editors.Add(new Editors.EditorCustom(nameof(LocalProperties.Amp2), typeof(LocalProperties), typeof(Amp)));
            PropertyLocal.Editors.Add(new Editors.EditorCustom(nameof(LocalProperties.BRD1), typeof(LocalProperties), typeof(BRD1)));
            PropertyLocal.Editors.Add(new Editors.EditorCustom(nameof(LocalProperties.BRD2), typeof(LocalProperties), typeof(BRD2)));
            PropertyLocal.Editors.Add(new Editors.EditorCustom(nameof(LocalProperties.GNSS), typeof(LocalProperties), typeof(GNSS)));
            PropertyLocal.Editors.Add(new Editors.EditorCustom(nameof(LocalProperties.PC), typeof(LocalProperties), typeof(PC)));
            PropertyLocal.Editors.Add(new Editors.EditorCustom(nameof(LocalProperties.GrozaR), typeof(LocalProperties), typeof(GrozaR)));
        }

        private void SetBindingAccess()
        {
            Binding binding = new Binding();
            binding.Source = Local.General;
            binding.Path = new PropertyPath(nameof(Local.General.Access));
            binding.Mode = BindingMode.TwoWay;
            binding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            BindingOperations.SetBinding(Global, GlobalProperties.AcssesProperty, binding);
        }


        private ViewCoord previewCoord;
        private void Local_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            
           switch (e.PropertyName)
            {
                case nameof(CommonLocal.Language):
                    ChangeLanguage(Local.General.Language);
                    //OnLanguageChanged(sender, savedLocal.General.Language);
                    OnLanguageChanged(sender, Local.General.Language);
                    
                    break;
                case nameof(CommonLocal.Access):
                    Console.Write($"Access: {Local.General.Access} \n");
                    Console.Write($"IsAccessChecked: {_isAccessChecked} \n");
                    if (Local.General.Access == AccessTypes.Admin)
                    {
                        CheckLocalAccess();
                        Console.WriteLine($"Acsess change to Admin");
                    }
                    else
                    {
                        ChangeVisibilityLocalProperties(false);
                        ChangeVisibilityGlobalProperties(false);
                        Console.WriteLine($"Acsess change to User");
                    }
                    Console.WriteLine(Local.General.Access);
                    break;

               case nameof(CommonLocal.CoordinateView):

                    VCFormat VCoordGNSS = new VCFormat(Global.GNSS.Latitude, Global.GNSS.Longitude);
                    VCFormat VCoordSpooffing = new VCFormat(Global.Spoofing.Latitude, Global.Spoofing.Longitude);           
                    TranslateCoordinate(VCoordGNSS, VCoordSpooffing);
                    Global.GNSS.ViewCoordField = Local.General.CoordinateView;
                    Global.Spoofing.ViewCoordField = Local.General.CoordinateView;
                    break;
                default:
                    break;
            }
        }

        private void TranslateCoordinate(VCFormat VCoordGNSS, VCFormat VCoordSpooffing)
        {
                    switch (Local.General.CoordinateView)
                    {
                        case ViewCoord.DMm:

                    if (Math.Round(Math.Abs(VCoordSpooffing.coordDegMin.Latitude.Minute), 4) >= 60)                    
                        Global.Spoofing.LatitudeStr = (VCoordSpooffing.coordDegMin.Latitude.Degree + 1).ToString() + "°" + CutRightZerosOfString(Math.Round(Math.Abs(VCoordSpooffing.coordDegMin.Latitude.Minute)-60, 4).ToString()) + "'";
                    else Global.Spoofing.LatitudeStr = (VCoordSpooffing.coordDegMin.Latitude.Degree).ToString() + "°" + CutRightZerosOfString(Math.Round(Math.Abs(VCoordSpooffing.coordDegMin.Latitude.Minute), 4).ToString()) + "'";
                    if (Math.Round(Math.Abs(VCoordGNSS.coordDegMin.Latitude.Minute), 4) >= 60)
                        Global.GNSS.LatitudeStr = (VCoordGNSS.coordDegMin.Latitude.Degree + 1).ToString() + "°" + CutRightZerosOfString(Math.Round(Math.Abs(VCoordGNSS.coordDegMin.Latitude.Minute)-60, 4).ToString()) + "'";                 
                    else Global.GNSS.LatitudeStr = (VCoordGNSS.coordDegMin.Latitude.Degree).ToString() + "°" + CutRightZerosOfString(Math.Round(Math.Abs(VCoordGNSS.coordDegMin.Latitude.Minute), 4).ToString()) + "'";
                    
                    if (Math.Round(Math.Abs(VCoordSpooffing.coordDegMin.Longitude.Minute), 4) >= 60)                   
                        Global.Spoofing.LongitudeStr = (VCoordSpooffing.coordDegMin.Longitude.Degree + 1).ToString() + "°" + CutRightZerosOfString(Math.Round(Math.Abs(VCoordSpooffing.coordDegMin.Longitude.Minute) - 60, 4).ToString()) + "'";
                    else Global.Spoofing.LongitudeStr = (VCoordSpooffing.coordDegMin.Longitude.Degree).ToString() + "°" + CutRightZerosOfString(Math.Round(Math.Abs(VCoordSpooffing.coordDegMin.Longitude.Minute), 4).ToString()) + "'";
                    if (Math.Round(Math.Abs(VCoordGNSS.coordDegMin.Longitude.Minute), 4) >= 60)
                        Global.GNSS.LongitudeStr = (VCoordGNSS.coordDegMin.Longitude.Degree + 1).ToString() + "°" + CutRightZerosOfString(Math.Round(Math.Abs(VCoordGNSS.coordDegMin.Longitude.Minute) - 60, 4).ToString()) + "'";                
                    else Global.GNSS.LongitudeStr = (VCoordGNSS.coordDegMin.Longitude.Degree).ToString() + "°" + CutRightZerosOfString(Math.Round(Math.Abs(VCoordGNSS.coordDegMin.Longitude.Minute), 4).ToString()) + "'";
                    
                    break;
                        case ViewCoord.DMSs:
                    if (Math.Round(Math.Abs(VCoordSpooffing.coordDegMinSec.Latitude.Second)) >= 60)                                           
                        Global.Spoofing.LatitudeStr = (VCoordSpooffing.coordDegMinSec.Latitude.Degree).ToString() + "°" + (Math.Abs(VCoordSpooffing.coordDegMinSec.Latitude.Minute)+1).ToString() + "'" + CutRightZerosOfString(Math.Round(Math.Abs(VCoordSpooffing.coordDegMinSec.Latitude.Second)-60).ToString()) + "\"";
                    else Global.Spoofing.LatitudeStr = (VCoordSpooffing.coordDegMinSec.Latitude.Degree).ToString() + "°" + (Math.Abs(VCoordSpooffing.coordDegMinSec.Latitude.Minute)).ToString() + "'" + CutRightZerosOfString(Math.Round(Math.Abs(VCoordSpooffing.coordDegMinSec.Latitude.Second)).ToString()) + "\"";
                    if (Math.Round(Math.Abs(VCoordGNSS.coordDegMinSec.Latitude.Second)) >= 60)
                        Global.GNSS.LatitudeStr = (VCoordGNSS.coordDegMinSec.Latitude.Degree).ToString() + "°" + (Math.Abs(VCoordGNSS.coordDegMinSec.Latitude.Minute)+1).ToString() + "'" + CutRightZerosOfString(Math.Round(Math.Abs(VCoordGNSS.coordDegMinSec.Latitude.Second)-60).ToString()) + "\"";                  
                    else Global.GNSS.LatitudeStr = (VCoordGNSS.coordDegMinSec.Latitude.Degree).ToString() + "°" + (Math.Abs(VCoordGNSS.coordDegMinSec.Latitude.Minute)).ToString() + "'" + CutRightZerosOfString(Math.Round(Math.Abs(VCoordGNSS.coordDegMinSec.Latitude.Second)).ToString()) + "\"";
                   
                    if (Math.Round(Math.Abs(VCoordSpooffing.coordDegMinSec.Longitude.Second)) >= 60)                    
                        Global.Spoofing.LongitudeStr = (VCoordSpooffing.coordDegMinSec.Longitude.Degree).ToString() + "°" + (Math.Abs(VCoordSpooffing.coordDegMinSec.Longitude.Minute) + 1).ToString() + "'" + CutRightZerosOfString(Math.Round(Math.Abs(VCoordSpooffing.coordDegMinSec.Longitude.Second) - 60).ToString()) + "\"";
                    else Global.Spoofing.LongitudeStr = (VCoordSpooffing.coordDegMinSec.Longitude.Degree).ToString() + "°" + (Math.Abs(VCoordSpooffing.coordDegMinSec.Longitude.Minute)).ToString() + "'" + CutRightZerosOfString(Math.Round(Math.Abs(VCoordSpooffing.coordDegMinSec.Longitude.Second)).ToString()) + "\"";
                    if (Math.Round(Math.Abs(VCoordGNSS.coordDegMinSec.Longitude.Second)) >= 60)
                        Global.GNSS.LongitudeStr = (VCoordGNSS.coordDegMinSec.Longitude.Degree).ToString() + "°" + (Math.Abs(VCoordGNSS.coordDegMinSec.Longitude.Minute) + 1).ToString() + "'" + CutRightZerosOfString(Math.Round(Math.Abs(VCoordGNSS.coordDegMinSec.Longitude.Second) - 60).ToString()) + "\"";
                    else Global.GNSS.LongitudeStr = (VCoordGNSS.coordDegMinSec.Longitude.Degree).ToString() + "°" + (Math.Abs(VCoordGNSS.coordDegMinSec.Longitude.Minute)).ToString() + "'" + CutRightZerosOfString(Math.Round(Math.Abs(VCoordGNSS.coordDegMinSec.Longitude.Second)).ToString()) + "\"";
                    
                    break;
                        case ViewCoord.Dd:
                            Global.Spoofing.LatitudeStr = CutRightZerosOfString(Math.Round(VCoordSpooffing.coordDeg.Latitude.Degree,6).ToString()) + "°";
                            Global.Spoofing.LongitudeStr = CutRightZerosOfString(Math.Round(VCoordSpooffing.coordDeg.Longitude.Degree,6).ToString()) + "°";
                            Global.GNSS.LatitudeStr = CutRightZerosOfString(Math.Round(VCoordGNSS.coordDeg.Latitude.Degree,6).ToString()) + "°";
                            Global.GNSS.LongitudeStr = CutRightZerosOfString(Math.Round(VCoordGNSS.coordDeg.Longitude.Degree,6).ToString()) + "°";
                            break;
                        default: break;
                    }                  
        }

        private string CutRightZerosOfString(string str)
        {
            if (str.Contains(',') == true)
                str.TrimEnd('0');
            return str;
        }

        private void SavedLocal_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case nameof(CommonLocal.Language):
                    OnLanguageChanged(sender, savedLocal.General.Language);
                    ChangeLanguage(savedLocal.General.Language);                   
                    break;
                
                default:
                    break;
            }

        }

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
           Validate();
        }

        #endregion

        public ControlProperties()
        {
            InitializeComponent();
            InitLocalProperties();
            InitGlobalProperties();
            ChangeLanguage(savedLocal.General.Language);
            InitAccessWindow();
            Local.General.CoordinateView = ViewCoord.Dd;
            previewCoord = Local.General.CoordinateView;
        }

        private void ApplyClick(object sender, RoutedEventArgs e)
        {
            if (IsLocalSelected)
            {
                if (!savedLocal.EqualTo(Local))
                {
                    savedLocal.Update(Local.Clone());
                    OnLocalPropertiesChanged(this, savedLocal.Clone());
                }
            }
            else
            {
                if (!savedGlobal.EqualTo(Global))
                {
                    savedGlobal.Update(Global.Clone());
                    OnGlobalPropertiesChanged(this, savedGlobal.Clone());
                }
            }
        }

      /*  private void NotApplyClick(object sender, RoutedEventArgs e)
        {
            if (IsLocalSelected)
            {
                if (!savedLocal.EqualTo(Local))
                {
                    Local.Update(savedLocal.Clone());
                    ChangeVisibilityLocalProperties(Local.General.Access == AccessTypes.Admin);
                    ChangeVisibilityGlobalProperties(Local.General.Access == AccessTypes.Admin); 
                }
            }
            else
            {
                if (!savedGlobal.EqualTo(Global))
                {
                    Global.Update(savedGlobal.Clone());
                }
            }
        }*/

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Validate();
        }

        private void ButDefault_Click(object sender, RoutedEventArgs e)
        {
            if (IsLocalSelected)
            {
                OnLocalDefaultButtonClick(this, null);
            }
            else
            {
                OnGlobalDefaultButtonClick(this, null);
            }
        }

        public void UpdateComPorts()
        {
            (Resources["PortsNames"] as ComPortsNames).UpdatePort();
        }

        public void ChangeVisibilityLocalProperties(bool browsable)
        {
            PropertyLocal.Properties[nameof(LocalProperties.DB)].IsBrowsable = browsable;
            PropertyLocal.Properties[nameof(LocalProperties.DF)].IsBrowsable = browsable;
            PropertyLocal.Properties[nameof(LocalProperties.RS)].IsBrowsable = browsable;
            PropertyLocal.Properties[nameof(LocalProperties.SS)].IsBrowsable = browsable;
            PropertyLocal.Properties[nameof(LocalProperties.LC)].IsBrowsable = browsable;
            PropertyLocal.Properties[nameof(LocalProperties.Amp1)].IsBrowsable = browsable;
            PropertyLocal.Properties[nameof(LocalProperties.Amp2)].IsBrowsable = browsable;
            PropertyLocal.Properties[nameof(LocalProperties.CmpRX)].IsBrowsable = browsable;
            PropertyLocal.Properties[nameof(LocalProperties.CmpTX)].IsBrowsable = browsable;
            PropertyLocal.Properties[nameof(LocalProperties.Rodnik)].IsBrowsable = browsable;
            PropertyLocal.Properties[nameof(LocalProperties.Cuirasse_M)].IsBrowsable = browsable;
            PropertyLocal.Properties[nameof(LocalProperties.Aeroscope)].IsBrowsable = browsable;
            PropertyLocal.Properties[nameof(LocalProperties.GrozaR)].IsBrowsable = browsable;


            if (browsable)
            {
                PropertyLocal.Properties[nameof(LocalProperties.PC)].IsBrowsable = true;
                PropertyLocal.Properties[nameof(LocalProperties.EOM)].IsBrowsable = true;
                PropertyLocal.Properties[nameof(LocalProperties.Lemt)].IsBrowsable = true;
                PropertyLocal.Properties[nameof(LocalProperties.BRD1)].IsBrowsable = true;
                PropertyLocal.Properties[nameof(LocalProperties.BRD2)].IsBrowsable = true;
                PropertyLocal.Properties[nameof(LocalProperties.Rodnik)].IsBrowsable = true;
                PropertyLocal.Properties[nameof(LocalProperties.Globus)].IsBrowsable = true;
                PropertyLocal.Properties[nameof(LocalProperties.GrozaR)].IsBrowsable = true;
            }
            else
            {
                PropertyLocal.Properties[nameof(LocalProperties.PC)].IsBrowsable = browsable ^ Local.PC.Existance;
                PropertyLocal.Properties[nameof(LocalProperties.EOM)].IsBrowsable = browsable ^ Local.EOM.Existance;
                PropertyLocal.Properties[nameof(LocalProperties.Lemt)].IsBrowsable = browsable ^ Local.Lemt.Existance;
                PropertyLocal.Properties[nameof(LocalProperties.Rodnik)].IsBrowsable = browsable ^ Local.Rodnik.Existance;
                PropertyLocal.Properties[nameof(LocalProperties.Globus)].IsBrowsable = browsable ^ Local.Globus.Existance;
                PropertyLocal.Properties[nameof(LocalProperties.BRD1)].IsBrowsable = browsable ^ Local.BRD1.Existance_Upper;
                PropertyLocal.Properties[nameof(LocalProperties.BRD2)].IsBrowsable = browsable ^ Local.BRD2.Existance_Upper;
               
            }

            if (!Local.EOM.Existance)
            {
                PropertyLocal.Properties[nameof(LocalProperties.BRD1)].IsBrowsable = browsable;
                PropertyLocal.Properties[nameof(LocalProperties.BRD2)].IsBrowsable = browsable;
            }
        }

        public void ChangeVisibilityGlobalProperties(bool browsable)
        {
            PropertyGlobal.Properties[nameof(GlobalProperties.EOM)].IsBrowsable = browsable;
            PropertyGlobal.Properties[nameof(GlobalProperties.Jamming)].IsBrowsable = browsable;
        }

        private void _this_Loaded(object sender, RoutedEventArgs e)
        {
            ChangeVisibilityLocalProperties(false);
            ChangeVisibilityGlobalProperties(false);
        }

        private void _this_Unloaded(object sender, RoutedEventArgs e)
        {
            _accessWindowPass.Close();
        }
    }
}
