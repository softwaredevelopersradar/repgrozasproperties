﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace DllGrozaSProperties.Converters
{
    public class SpoofingTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (byte)value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (Models.TypeSpoofing)System.Convert.ToByte(value);
        }
    }
}
