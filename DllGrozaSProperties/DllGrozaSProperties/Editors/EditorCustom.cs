﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;
using DllGrozaSProperties.Models;
using DllGrozaSProperties.Models.Global;
using DllGrozaSProperties.Models.Local;

namespace DllGrozaSProperties.Editors
{
    public class EditorCustom : PropertyEditor
    {
        Dictionary<Type, string> dictKeyDataTemplate = new Dictionary<Type, string>()
        {
            { typeof(EndPointConnection), "EndPointEditorKey"},
            { typeof(CommonLocal), "CommonLocalEditorKey"},
            { typeof(UdpConnection), "UdpPointEditorKey" },
            { typeof(Rodnik) , "RodnikEditorKey" },
            { typeof(Globus), "GlobusEditorKey" },
            { typeof(CommonGlobal), "CommonGlobalEditorKey" },
            { typeof(RadioIntelegence), "RadioIntelegenceEditorKey" },
            { typeof(OemGlobal), "OemEditorKey"},
            { typeof(CmpGlobal), "CmpGlobalEditorKey"},
            { typeof(Jamming), "JammingEditorKey" },
            { typeof(Lemt), "LemtEditorKey" },
            { typeof(Map),  "MapEditorKey"},
            { typeof(Cmp), "CmpEditorKey" },
            { typeof(BRD1), "BRD1EditorKey" },
            { typeof(BRD2), "BRD2EditorKey" },
            { typeof(GNSS), "GNSSEditorKey" },
            { typeof(EDConnection), "EDEditorKey" },
            { typeof(RS), "RSEditorKey" },
            { typeof(SS), "SSEditorKey" },
            { typeof(LC), "LCEditorKey"},
            { typeof(PC), "PCEditorKey" },
            { typeof(DF), "DFEditorKey" },
            { typeof(Amp), "AmpEditorKey"},
            { typeof(CuirasseM), "CuirasseMEditorKey"},
            { typeof(Aeroscope), "AeroscopeEditorKey"},
            { typeof(GrozaR), "GrozaREditorKey" },
        };

        public EditorCustom(string PropertyName, Type DeclaringType, Type typeProperty)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/DllGrozaSProperties;component/Editors/Templates.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            this.InlineTemplate = resource[dictKeyDataTemplate[typeProperty]];
        }

        public EditorCustom(string PropertyName, Type DeclaringType, string nameEditorKey)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/DllGrozaSProperties;component/Editors/Templates.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            this.InlineTemplate = resource[nameEditorKey];
        }
    }
}
