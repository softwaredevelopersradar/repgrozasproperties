﻿using System.ComponentModel.DataAnnotations;

namespace DllGrozaSProperties.Models.Global
{
    public class CmpGlobal : AbstractBaseProperties<CmpGlobal>
    {
        #region IModelMethods

        public override CmpGlobal Clone()
        {
            return new CmpGlobal
            {
                Angle = Angle
            };
        }

        public override bool EqualTo(CmpGlobal model)
        {
            return Angle == model.Angle;
        }

        public override void Update(CmpGlobal model)
        {
            Angle = model.Angle;        }

        #endregion

        private float angle = 0;

        [Range(0, 359)]
        public float Angle
        {
            get => angle;
            set
            {
                if (value == angle) return;
                angle = value;
                OnPropertyChanged();
            }
        }
    }
}
