﻿namespace DllGrozaSProperties.Models
{
    public class CommonGlobal : AbstractBaseProperties<CommonGlobal>
    {
        #region IModelMethods

        public override CommonGlobal Clone()
        {
            return new CommonGlobal
            {
                AutoMode = AutoMode,
                CompassJ = CompassJ,
                CompassRI = CompassRI,
                Latitude = Latitude,
                Longitude = Longitude,
                Num = Num
            };
        }

        public override bool EqualTo(CommonGlobal model)
        {
            return Num == model.Num
                && Longitude == model.Longitude
                && Latitude == model.Latitude
                && CompassRI == model.CompassRI
                && CompassJ == model.CompassJ
                && AutoMode == model.AutoMode;
        }

        public override void Update(CommonGlobal model)
        {
            AutoMode = model.AutoMode;
            CompassJ = model.CompassJ;
            CompassRI = model.CompassRI;
            Latitude = model.Latitude;
            Longitude = model.Longitude;
            Num = model.Num;
        }

        #endregion        

        private short num;
        private float compassRI;
        private float compassJ;
        private double latitude;
        private double longitude;
        private bool autoMode;

        public short Num
        {
            get => num;
            set
            {
                if (num == value) return;
                num = value;
                OnPropertyChanged();
            }
        }

        public float CompassRI
        {
            get => compassRI;
            set
            {
                if (compassRI == value) return;
                compassRI = value;
                OnPropertyChanged();
            }
        }

        public float CompassJ
        {
            get => compassJ;
            set
            {
                if (compassJ == value) return;
                compassJ = value;
                OnPropertyChanged();

            }
        }

        public double Latitude
        {
            get => latitude;
            set
            {
                if (latitude == value) return;
                latitude = value;
                OnPropertyChanged();

            }
        }

        public double Longitude
        {
            get => longitude;
            set
            {
                if (longitude == value) return;
                longitude = value;
                OnPropertyChanged();
            }
        }

        public bool AutoMode
        {
            get => autoMode;
            set
            {
                if (autoMode == value) return;
                autoMode = value;
                OnPropertyChanged();
            }
        }
    }
}
