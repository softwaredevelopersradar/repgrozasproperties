﻿using DllGrozaSProperties.Models.Global;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;
using YamlDotNet.Serialization;

namespace DllGrozaSProperties.Models
{
    [CategoryOrder(nameof(GNSS), 1)]
    [CategoryOrder(nameof(CmpRX), 2)]
    [CategoryOrder(nameof(CmpTX), 3)]
    [CategoryOrder(nameof(EOM), 4)]
    [CategoryOrder(nameof(RadioIntelegence), 5)]
    [CategoryOrder(nameof(Jamming), 6)]
    [CategoryOrder(nameof(Spoofing), 7)]
    public class GlobalProperties: DependencyObject, IModelMethods<GlobalProperties>
    {
        #region IModelMethods

        public GlobalProperties Clone()
        {
            return new GlobalProperties
            {
                Jamming = Jamming.Clone(),
                GNSS = GNSS.Clone(),
                CmpRX = CmpRX.Clone(),
                CmpTX = CmpTX.Clone(),
                EOM = EOM.Clone(),
                Spoofing = Spoofing.Clone(),
                RadioIntelegence = RadioIntelegence.Clone()
            };
        }

        public bool EqualTo(GlobalProperties model)
        {
            return Jamming.EqualTo(model.Jamming)
                && RadioIntelegence.EqualTo(model.RadioIntelegence)
                && GNSS.EqualTo(model.GNSS)
                && CmpRX.EqualTo(model.CmpRX)
                && CmpTX.EqualTo(model.CmpTX)
                && EOM.EqualTo(model.EOM)
                && Spoofing.EqualTo(model.Spoofing);
        }

        public void Update(GlobalProperties model)
        {
            Jamming.Update(model.Jamming);
            RadioIntelegence.Update(model.RadioIntelegence);
            GNSS.Update(model.GNSS);
            CmpRX.Update(model.CmpRX);
            CmpTX.Update(model.CmpTX);
            EOM.Update(model.EOM);
            Spoofing.Update(model.Spoofing);
        }

        #endregion

        public event PropertyChangedEventHandler OnPropertyChanged = (obg, str) => { };

        public GlobalProperties()
        {
            Jamming = new Jamming();
            RadioIntelegence = new RadioIntelegence();
            GNSS = new CoordGloabal();
            CmpRX = new CmpGlobal();
            CmpTX = new CmpGlobal();
            EOM = new OemGlobal();
            Spoofing = new CoordGloabal();
            //Common.PropertyChanged += PropertyChanged;
            Jamming.PropertyChanged += PropertyChanged;
            RadioIntelegence.PropertyChanged += PropertyChanged;
            GNSS.PropertyChanged += PropertyChanged;
            CmpRX.PropertyChanged += PropertyChanged;
            CmpTX.PropertyChanged += PropertyChanged;
            EOM.PropertyChanged += PropertyChanged;
            Spoofing.PropertyChanged += PropertyChanged;
        }

        private void PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged(this, e);
        }

        [Category(nameof(RadioIntelegence))]
        [DisplayName(" ")]
        public RadioIntelegence RadioIntelegence { get; set; }

        [Category(nameof(Jamming))]
        [DisplayName(" ")]
        public Jamming Jamming { get; set; }

        [Category(nameof(GNSS))]
        [DisplayName(" ")]
        public CoordGloabal GNSS { get; set; } 

        [Category(nameof(CmpRX))]
        [DisplayName(" ")]
        public CmpGlobal CmpRX { get; set; }

        [Category(nameof(CmpTX))]
        [DisplayName(" ")]
        public CmpGlobal CmpTX { get; set; }

        [Category(nameof(EOM))]
        [DisplayName(" ")]
        public OemGlobal EOM { get; set; }

        [Category(nameof(Spoofing))]
        [DisplayName(" ")]
        public CoordGloabal Spoofing { get; set; }

        public static readonly DependencyProperty AcssesProperty = DependencyProperty.Register(nameof(Access), typeof(AccessTypes), typeof(GlobalProperties), new PropertyMetadata(AccessTypes.Admin, new PropertyChangedCallback(AccessChanged)));

        static void AccessChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            GlobalProperties control = (GlobalProperties)d;
            control.Access = (AccessTypes)e.NewValue;
        }

        [YamlIgnore]
        [Browsable(false)]
        public AccessTypes Access
        {
            get { return (AccessTypes)GetValue(AcssesProperty); }
            set { SetValue(AcssesProperty, value); }
        }

    }
}
