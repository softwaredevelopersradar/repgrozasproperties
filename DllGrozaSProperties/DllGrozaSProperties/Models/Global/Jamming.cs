﻿using System.ComponentModel.DataAnnotations;

namespace DllGrozaSProperties.Models
{
    public class Jamming : AbstractBaseProperties<Jamming>
    {
        #region IModelMethods

        public override Jamming Clone()
        {
            return new Jamming
            {
                Power100_500 = Power100_500,
                Power500_2500 = Power500_2500,
                Power2500_6000 = Power2500_6000,
                TimeRadiat = TimeRadiat,
                Distance = Distance,
                Sector = Sector
            };
        }

        public override bool EqualTo(Jamming model)
        {
            return TimeRadiat == model.TimeRadiat
                && Power100_500 == model.Power100_500
                && Power500_2500 == model.Power500_2500
                && Power2500_6000 == model.Power2500_6000
                && Distance == model.Distance
                && Sector == model.Sector;
        }

        public override void Update(Jamming model)
        {
            TimeRadiat = model.TimeRadiat;
            Power100_500 = model.Power100_500;
            Power500_2500 = model.Power500_2500;
            Power2500_6000 = model.Power2500_6000;
            Distance = model.Distance;
            Sector = model.Sector;
        }
        #endregion

        private int timeRadiat;
        private int power100_500 = 125;
        private int power500_2500 = 50;
        private int power2500_6000 = 100;
        private int _distance;
        private float _sector;


        public int TimeRadiat
        {
            get => timeRadiat;
            set
            {
                if (timeRadiat == value) return;
                timeRadiat = value;
                OnPropertyChanged();
            }
        }

        [Range(10, 125)]
        public int Power100_500
        {
            get => power100_500;
            set
            {
                if (power100_500 == value) return;
                power100_500 = value;
                OnPropertyChanged();
            }
        }

        [Range(10, 50)]
        public int Power500_2500
        {
            get => power500_2500;
            set
            {
                if (power500_2500 == value) return;
                power500_2500 = value;
                OnPropertyChanged();
            }
        }

        [Range(10, 100)]
        public int Power2500_6000
        {
            get => power2500_6000;
            set
            {
                if (power2500_6000 == value) return;
                power2500_6000 = value;
                OnPropertyChanged();
            }
        }

        [Range(0, 100000)]
        public int Distance
        {
            get => _distance;
            set
            {
                if (_distance == value) return;
                _distance = value;
                OnPropertyChanged();
            }
        }

        [Range(0, 360)]
        public float Sector
        {
            get => _sector;
            set
            {
                if (_sector == value) return;
                _sector = value;
                OnPropertyChanged();
            }
        }
    }
}
