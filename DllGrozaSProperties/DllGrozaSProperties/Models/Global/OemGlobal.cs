﻿using System.ComponentModel.DataAnnotations;

namespace DllGrozaSProperties.Models.Global
{
    public class OemGlobal : AbstractBaseProperties<OemGlobal>
    {
        #region IModelMethods

        public override OemGlobal Clone()
        {
            return new OemGlobal
            {
                Distance = Distance
            };
        }

        public override bool EqualTo(OemGlobal model)
        {
            return Distance == model.Distance;
        }

        public override void Update(OemGlobal model)
        {
            Distance = model.Distance;
        }
        #endregion

        private int _distance = 7000;

      
        [Range(0, 65000)]
        public int Distance
        {
            get => _distance;
            set
            {
                if (_distance == value) return;
                _distance = value;
                OnPropertyChanged();
            }
        }
    }
}
