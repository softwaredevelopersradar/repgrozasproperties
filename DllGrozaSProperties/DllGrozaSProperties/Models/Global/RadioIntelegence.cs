﻿using DllGrozaSProperties.Models.Global;
using System.ComponentModel.DataAnnotations;


namespace DllGrozaSProperties.Models
{
    public class RadioIntelegence : AbstractBaseProperties<RadioIntelegence>
    {
        #region IModelMethods

        public override RadioIntelegence Clone()
        {
            return new RadioIntelegence
            {               
                ZoneAlarm = ZoneAlarm,
                ZoneAttention = ZoneAttention,
                ZoneReadiness = ZoneReadiness,
                Distance = Distance,
                BearingAccuracy = BearingAccuracy,
                StaticAngle = StaticAngle,
                FrequencyAccuracy = FrequencyAccuracy,
                BandAccuracy = BandAccuracy,
                BandLargerError = BandLargerError,
                BandLessError = BandLessError,
                NumberOfScans = NumberOfScans,
                ScanningChannels = ScanningChannels,
                DetectionChannel = DetectionChannel
            };
        }

        public override bool EqualTo(RadioIntelegence model)
        {
            return ZoneReadiness == model.ZoneReadiness
                && ZoneAttention == model.ZoneAttention
                && ZoneAlarm == model.ZoneAlarm               
                && Distance == model.Distance
                && BearingAccuracy == model.BearingAccuracy
                && StaticAngle == model.StaticAngle
                && FrequencyAccuracy == model.FrequencyAccuracy
                && BandAccuracy==model.BandAccuracy
                && BandLessError == model.BandLessError
                && BandLargerError == model.BandLargerError 
                && NumberOfScans == model.NumberOfScans
                && ScanningChannels == model.ScanningChannels
                && DetectionChannel == model.DetectionChannel;
        }

        public override void Update(RadioIntelegence model)
        {          
            ZoneAlarm = model.ZoneAlarm;
            ZoneAttention = model.ZoneAttention;
            ZoneReadiness = model.ZoneReadiness;
            Distance = model.Distance;
            BearingAccuracy = model.BearingAccuracy;
            StaticAngle = model.StaticAngle;
            FrequencyAccuracy = model.FrequencyAccuracy;
            BandAccuracy = model.BandAccuracy;
            BandLargerError = model.BandLargerError;    
            BandLessError = model.BandLessError;    
            NumberOfScans = model.NumberOfScans;
            ScanningChannels = model.ScanningChannels;
            DetectionChannel = model.DetectionChannel;
        }
        #endregion
      
        private int _zoneAttention = 50000;
        private int _zoneReadiness = 20000;
        private int _zoneAlarm = 10000;
        private int _distance = 50000;
        private float _bearingAccuracy = 22.5f;
        private float _staticAngle = -1;
        private float _frequencyAccuracy = 10.0f;
        private float _bandAccuracy = 10.0f;
        private float _bandLargerError = 10.0f;
        private float _bandLessError = 10.0f;
        private byte _numberOfScans = 6;
        private SCANCH _scanningChannels = SCANCH.One;
        private SCANCH _detectionChanel = SCANCH.One;

        [Range(0, 65000)]
        public int ZoneAttention
        {
            get => _zoneAttention;
            set
            {
                if (_zoneAttention == value) return;
                _zoneAttention = value;
                OnPropertyChanged();
            }
        }

        [Range(0, 65000)]
        public int ZoneReadiness
        {
            get => _zoneReadiness;
            set
            {
                if (_zoneReadiness == value) return;
                _zoneReadiness = value;
                OnPropertyChanged();
            }
        }

        [Range(0, 65000)]
        public int ZoneAlarm
        {
            get => _zoneAlarm;
            set
            {
                if (_zoneAlarm == value) return;
                _zoneAlarm = value;
                OnPropertyChanged();
            }
        }

        [Range(0, 500000)]
        public int Distance
        {
            get => _distance;
            set
            {
                if (_distance == value) return;
                _distance = value;
                OnPropertyChanged();
            }
        }

        [Range(0, 180)]
        public float BearingAccuracy
        {
            get => _bearingAccuracy;
            set
            {
                if (_bearingAccuracy == value) return;
                _bearingAccuracy = value;
                OnPropertyChanged();
            }
        }

        [Range(-1, 359)]
        public float StaticAngle
        {
            get => _staticAngle;
            set
            {
                if (_staticAngle == value) return;
                _staticAngle = value;
                OnPropertyChanged();
            }
        }

        [Range(0.1, 100)]
        public float FrequencyAccuracy
        {
            get => _frequencyAccuracy;
            set
            {
                if (_frequencyAccuracy == value) return;
                _frequencyAccuracy = value;
                OnPropertyChanged();
            }
        }

        [Range(0.1, 100)]
        public float BandAccuracy
        {
            get => _bandAccuracy;
            set
            {
                if (_bandAccuracy == value) return;
                _bandAccuracy = value;
                OnPropertyChanged();
            }
        }


        [Range(0.1, 100)]
        public float BandLargerError
        {
            get => _bandLargerError;
            set
            {
                if (_bandLargerError == value) return;
                _bandLargerError = value;
                OnPropertyChanged();
            }
        }


        [Range(0.1, 100)]
        public float BandLessError
        {
            get => _bandLessError;
            set
            {
                if (_bandLessError == value) return;
                _bandLessError = value;
                OnPropertyChanged();
            }
        }

        [Range(1, 50)]
        public byte NumberOfScans
        {
            get => _numberOfScans;
            set
            {
                if (_numberOfScans == value) return;
                _numberOfScans = value;
                OnPropertyChanged();
            }
        }

        public SCANCH ScanningChannels
        {
            get => _scanningChannels;
            set
            {
                if (_scanningChannels == value) return;
                _scanningChannels = value;
                OnPropertyChanged();
            }
        }


        public SCANCH DetectionChannel
        {
            get => _detectionChanel;
            set
            {
                if (_detectionChanel == value) return;
                _detectionChanel = value;
                OnPropertyChanged();
            }
        }

    }
}
