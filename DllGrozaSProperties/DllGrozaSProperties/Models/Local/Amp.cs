﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DllGrozaSProperties.Models.Local
{
    public class Amp : AbstractBaseProperties<Amp>
    {
        #region IModelMethods

        public override Amp Clone()
        {
            return new Amp
            {
                ComPort = ComPort,
                Existance = Existance,
            };
        }

        public override bool EqualTo(Amp model)
        {
            return ComPort == model.ComPort
                && Existance == model.Existance;
        }

        public override void Update(Amp model)
        {
            ComPort = model.ComPort;
            Existance = model.Existance;
        }

        #endregion

        private bool existance;
        private string comPort = " ";

        public bool Existance
        {
            get => existance;
            set
            {
                if (existance == value) return;
                existance = value;
                OnPropertyChanged();
            }

        }

        public string ComPort
        {
            get => comPort;
            set
            {
                if (value == comPort) return;
                comPort = value;
                OnPropertyChanged();
            }
        }
    }
}
