﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace DllGrozaSProperties.Models.Local
{
    public class BRD1 : AbstractBaseProperties<BRD1>
    {
        #region ModelMethods

        public override bool EqualTo(BRD1 model)
        {
            return ComPort == model.ComPort
                && Existance == model.Existance
                && Type == model.Type
                && TransportAngle_Upper == model.TransportAngle_Upper          
                && Existance_Upper == model.Existance_Upper
                && Address_Upper == model.Address_Upper
                && CompassAngle_Upper == model.CompassAngle_Upper
                && TransportAngle_Bottom == model.TransportAngle_Bottom
                && Existance_Bottom == model.Existance_Bottom
                && Address_Bottom == model.Address_Bottom
                && CompassAngle_Bottom == model.CompassAngle_Bottom
                && SynchronizeEOM_Upper == model.SynchronizeEOM_Upper
                && ForbiddenAngleMin_Upper == model.ForbiddenAngleMin_Upper
                && ForbiddenAngleMax_Upper == model.ForbiddenAngleMax_Upper
                && RotationSpeed_Upper == model.RotationSpeed_Upper
                && RotationAcceleration_Upper == model.RotationAcceleration_Upper;
        }

        public override void Update(BRD1 model)
        {
            ComPort = model.ComPort;
            Type = model.Type;
            Existance = model.Existance;
            TransportAngle_Upper = model.TransportAngle_Upper;
            Existance_Upper = model.Existance_Upper;
            Address_Upper = model.Address_Upper;
            CompassAngle_Upper = model.CompassAngle_Upper;
            TransportAngle_Bottom = model.TransportAngle_Bottom;
            Existance_Bottom = model.Existance_Bottom;
            Address_Bottom = model.Address_Bottom;
            CompassAngle_Bottom = model.CompassAngle_Bottom;
            SynchronizeEOM_Upper = model.SynchronizeEOM_Upper;
            ForbiddenAngleMin_Upper = model.ForbiddenAngleMin_Upper;
            ForbiddenAngleMax_Upper = model.ForbiddenAngleMax_Upper;
            RotationSpeed_Upper = model.RotationSpeed_Upper;
            RotationAcceleration_Upper = model.RotationAcceleration_Upper;
        }

        public override BRD1 Clone()
        {
            return new BRD1
            {
                ComPort = ComPort,
                Existance = Existance,
                Type = Type,
                Address_Upper = Address_Upper,
                TransportAngle_Upper = TransportAngle_Upper,
                Existance_Upper = Existance_Upper,
                CompassAngle_Upper = CompassAngle_Upper,
                Address_Bottom = Address_Bottom,
                TransportAngle_Bottom = TransportAngle_Bottom,
                Existance_Bottom = Existance_Bottom,
                CompassAngle_Bottom = CompassAngle_Bottom,
                SynchronizeEOM_Upper = SynchronizeEOM_Upper,
                ForbiddenAngleMin_Upper = ForbiddenAngleMin_Upper,
                ForbiddenAngleMax_Upper = ForbiddenAngleMax_Upper,
                RotationSpeed_Upper = RotationSpeed_Upper,
                RotationAcceleration_Upper = RotationAcceleration_Upper
            };
        }

        #endregion
        private string _comPort = " ";
        private bool _existance;
        private TypeBRD typeBRD = TypeBRD._2TS;

        private short _address_1 = 12;
        private bool _existance_1;
        private float _transportAngle_1;
        private float _compassAngle_1;

        private float _forbiddenAngleMin_1;
        private float _forbiddenAngleMax_1;
        private int _speed_1 = 50;
        private int _acceleration_1 = 100;

        private short _address_2 = 12;
        private bool _existance_2;
        private float _transportAngle_2;
        private float _compassAngle_2;

        private bool _synchronizeEOM = true;
        public string ComPort
        {
            get => _comPort;
            set
            {
                if (value == _comPort) return;
                _comPort = value;
                OnPropertyChanged();
            }
        }

        [Browsable(false)]
        public bool Existance
        {
            get => _existance;
            set
            {
                if (value == _existance) return;
                _existance = value;
                OnPropertyChanged();
            }
        }

        public TypeBRD Type
        {
            get => typeBRD;
            set
            {
                if(typeBRD == value) return;
                typeBRD = value;
                OnPropertyChanged();
            }
        }

        public bool Existance_Upper
        {
            get => _existance_1;
            set
            {
                if (_existance_1 == value) return;
                _existance_1 = value;

                if (_existance_1)
                    Existance = true;
                if (!_existance_1 && !_existance_2)
                    Existance = false;
                OnPropertyChanged();
            }
        }

        [Range(0, 359)]
        public float TransportAngle_Upper
        {
            get => _transportAngle_1;
            set
            {
                if (value == _transportAngle_1) return;
                _transportAngle_1 = value;
                OnPropertyChanged();
            }
        }

        [Range(0, 359)]
        public float CompassAngle_Upper
        {
            get => _compassAngle_1;
            set
            {
                if (value == _compassAngle_1) return;
                _compassAngle_1 = value;
                OnPropertyChanged();
            }
        }

        [Range(0, 359)]
        public float ForbiddenAngleMin_Upper
        {
            get => _forbiddenAngleMin_1;
            set
            {
                if (value == _forbiddenAngleMin_1) return;
                _forbiddenAngleMin_1 = value;
                OnPropertyChanged();
            }
        }

        [Range(0, 359)]
        public float ForbiddenAngleMax_Upper
        {
            get => _forbiddenAngleMax_1;
            set
            {
                if (value == _forbiddenAngleMax_1) return;
                _forbiddenAngleMax_1 = value;
                OnPropertyChanged();
            }
        }

        [Range(0, 255)]
        public int RotationSpeed_Upper
        {
            get => this._speed_1;
            set
            {
                if (value == _speed_1) return;
                _speed_1 = value;
                OnPropertyChanged();
            }
        }

        [Range(0, 255)]
        public int RotationAcceleration_Upper
        {
            get => this._acceleration_1;
            set
            {
                if (value == _acceleration_1) return;
                _acceleration_1 = value;
                OnPropertyChanged();
            }
        }

        [Range(1, 24)]
        public short Address_Upper
        {
            get => _address_1;
            set
            {
                if (_address_1 == value) return;
                _address_1 = value;
                OnPropertyChanged();
            }
        }

        public bool Existance_Bottom
        {
            get => _existance_2;
            set
            {
                if (_existance_2 == value) return;
                _existance_2 = value;
                if (_existance_2)
                    Existance = true;
                if (!_existance_1 && !_existance_2)
                    Existance = false;
                OnPropertyChanged();
            }
        }

        [Range(0, 359)]
        public float TransportAngle_Bottom
        {
            get => _transportAngle_2;
            set
            {
                if (value == _transportAngle_2) return;
                _transportAngle_2 = value;
                OnPropertyChanged();
            }
        }

        [Range(0, 359)]
        public float CompassAngle_Bottom
        {
            get => _compassAngle_2;
            set
            {
                if (value == _compassAngle_2) return;
                _compassAngle_2 = value;
                OnPropertyChanged();
            }
        }

        [Range(1, 24)]
        public short Address_Bottom
        {
            get => _address_2;
            set
            {
                if (_address_2 == value) return;
                _address_2 = value;
                OnPropertyChanged();
            }
        }

        public bool SynchronizeEOM_Upper
        {
            get => _synchronizeEOM;
            set
            {
                if (_synchronizeEOM == value) return;
                _synchronizeEOM = value;
                OnPropertyChanged();
            }
        }
    }
}
