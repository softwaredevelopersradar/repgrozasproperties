﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DllGrozaSProperties.Models.Local
{
    public class BRD2 : AbstractBaseProperties<BRD2>
    {
        #region ModelMethods

        public override bool EqualTo(BRD2 model)
        {
            return ComPort == model.ComPort
                && Existance == model.Existance
                && TransportAngle_Upper == model.TransportAngle_Upper
                && Existance_Upper == model.Existance_Upper
                && Address_Upper == model.Address_Upper
                && CompassAngle_Upper == model.CompassAngle_Upper
                && TransportAngle_Bottom == model.TransportAngle_Bottom
                && Existance_Bottom == model.Existance_Bottom
                && Address_Bottom == model.Address_Bottom
                && CompassAngle_Bottom == model.CompassAngle_Bottom
                && SynchronizeEOM_Upper == model.SynchronizeEOM_Upper;
        }

        public override void Update(BRD2 model)
        {
            ComPort = model.ComPort;
            Existance = model.Existance;
            TransportAngle_Upper = model.TransportAngle_Upper;
            Existance_Upper = model.Existance_Upper;
            Address_Upper = model.Address_Upper;
            CompassAngle_Upper = model.CompassAngle_Upper;
            TransportAngle_Bottom = model.TransportAngle_Bottom;
            Existance_Bottom = model.Existance_Bottom;
            Address_Bottom = model.Address_Bottom;
            CompassAngle_Bottom = model.CompassAngle_Bottom;
            SynchronizeEOM_Upper = model.SynchronizeEOM_Upper;
        }

        public override BRD2 Clone()
        {
            return new BRD2
            {
                ComPort = ComPort,
                Existance = Existance,
                Address_Upper = Address_Upper,
                TransportAngle_Upper = TransportAngle_Upper,
                Existance_Upper = Existance_Upper,
                CompassAngle_Upper = CompassAngle_Upper,
                Address_Bottom = Address_Bottom,
                TransportAngle_Bottom = TransportAngle_Bottom,
                Existance_Bottom = Existance_Bottom,
                CompassAngle_Bottom = CompassAngle_Bottom,
                SynchronizeEOM_Upper = SynchronizeEOM_Upper
            };
        }

        #endregion
        private string _comPort = " ";
        private bool _existance;

        private short _address_1 = 12;
        private bool _existance_1;
        private float _transportAngle_1;
        private float _compassAngle_1;

        private short _address_2 = 12;
        private bool _existance_2;
        private float _transportAngle_2;
        private float _compassAngle_2;

        private bool _synchronizeEOM = true;

        public string ComPort
        {
            get => _comPort;
            set
            {
                if (value == _comPort) return;
                _comPort = value;
                OnPropertyChanged();
            }
        }

        [Browsable(false)]
        public bool Existance
        {
            get => _existance;
            set
            {
                if (value == _existance) return;
                _existance = value;
                OnPropertyChanged();
            }
        }



        public bool Existance_Upper
        {
            get => _existance_1;
            set
            {
                if (_existance_1 == value) return;
                _existance_1 = value;

                if (_existance_1)
                    Existance = true;
                if (!_existance_1 && !_existance_2)
                    Existance = false;
                OnPropertyChanged();
            }
        }

        [Range(0, 359)]
        public float TransportAngle_Upper
        {
            get => _transportAngle_1;
            set
            {
                if (value == _transportAngle_1) return;
                _transportAngle_1 = value;
                OnPropertyChanged();
            }
        }

        [Range(0, 359)]
        public float CompassAngle_Upper
        {
            get => _compassAngle_1;
            set
            {
                if (value == _compassAngle_1) return;
                _compassAngle_1 = value;
                OnPropertyChanged();
            }
        }

        [Range(1, 24)]
        public short Address_Upper
        {
            get => _address_1;
            set
            {
                if (_address_1 == value) return;
                _address_1 = value;
                OnPropertyChanged();
            }
        }


        public bool Existance_Bottom
        {
            get => _existance_2;
            set
            {
                if (_existance_2 == value) return;
                _existance_2 = value;
                if (_existance_2)
                    Existance = true;
                if (!_existance_1 && !_existance_2)
                    Existance = false;
                OnPropertyChanged();
            }
        }

        [Range(0, 359)]
        public float TransportAngle_Bottom
        {
            get => _transportAngle_2;
            set
            {
                if (value == _transportAngle_2) return;
                _transportAngle_2 = value;
                OnPropertyChanged();
            }
        }

        [Range(0, 359)]
        public float CompassAngle_Bottom
        {
            get => _compassAngle_2;
            set
            {
                if (value == _compassAngle_2) return;
                _compassAngle_2 = value;
                OnPropertyChanged();
            }
        }

        [Range(1, 24)]
        public short Address_Bottom
        {
            get => _address_2;
            set
            {
                if (_address_2 == value) return;
                _address_2 = value;
                OnPropertyChanged();
            }
        }


        public bool SynchronizeEOM_Upper
        {
            get => _synchronizeEOM;
            set
            {
                if (_synchronizeEOM == value) return;
                _synchronizeEOM = value;
                OnPropertyChanged();
            }
        }
    }
}
