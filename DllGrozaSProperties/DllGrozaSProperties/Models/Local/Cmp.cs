﻿using System.ComponentModel.DataAnnotations;

namespace DllGrozaSProperties.Models.Local
{
    public class Cmp : AbstractBaseProperties<Cmp>
    {
        #region IModelMethods

        public override Cmp Clone()
        {
            return new Cmp
            {
                ComPort = ComPort,
                Existance = Existance,
                ErrorDeg = ErrorDeg
            };
        }

        public override bool EqualTo(Cmp model)
        {
            return ComPort == model.ComPort
                && Existance == model.Existance
                && ErrorDeg == model.ErrorDeg;
        }

        public override void Update(Cmp model)
        {
            ComPort = model.ComPort;
            Existance = model.Existance;
            ErrorDeg = model.ErrorDeg;
        }

        #endregion

        private bool existance;
        private string comPort = " ";
        private float errorDeg = 0;

        public bool Existance
        {
            get => existance;
            set
            {
                if (existance == value) return;
                existance = value;
                OnPropertyChanged();
            }

        }

        public string ComPort
        {
            get => comPort;
            set
            {
                if (value == comPort) return;
                comPort = value;
                OnPropertyChanged();
            }
        }

        [Range(0, 359)]
        public float ErrorDeg
        {
            get => errorDeg;
            set
            {
                if (errorDeg == value) return;
                errorDeg = value;
                OnPropertyChanged();
            }
        }
    }
}
