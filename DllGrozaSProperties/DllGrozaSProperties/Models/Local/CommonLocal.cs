﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DllGrozaSProperties.Models
{
    public class CommonLocal: AbstractBaseProperties<CommonLocal>
    { 
        #region IModelMethods

        public override bool EqualTo(CommonLocal model)
        {
            return Access == model.Access
                && Language == model.Language
                && AWS == model.AWS
                && FolderImagesDrones == model.FolderImagesDrones
                && AudioFileRI == model.AudioFileRI
                && AudioFileJ == model.AudioFileJ
                && FolderRI_UAV == model.FolderRI_UAV
                && IsVisibleEN == model.IsVisibleEN
                && IsVisibleRU == model.IsVisibleRU
                && IsVisibleAZ == model.IsVisibleAZ
                && IsVisibleSR == model.IsVisibleSR
                && CoordinateView == model.CoordinateView
                && AudioFileRadar == model.AudioFileRadar
                && AudioRIDurationSec == model.AudioRIDurationSec
                && ScreenshotsLivetimeDays == model.ScreenshotsLivetimeDays
                && ScreenshotsStorageMB == model.ScreenshotsStorageMB;
        }

        public override CommonLocal Clone()
        {
            return new CommonLocal
            {
                Access = Access,
                Language = Language,
                AWS = AWS,
                FolderImagesDrones = FolderImagesDrones,
                AudioFileRI = AudioFileRI,
                AudioFileJ = AudioFileJ,
                FolderRI_UAV = FolderRI_UAV,
                CoordinateView = CoordinateView,
                IsVisibleEN = IsVisibleEN,
                IsVisibleRU = IsVisibleRU,
                IsVisibleAZ = IsVisibleAZ,
                IsVisibleSR = IsVisibleSR,
                AudioFileRadar = AudioFileRadar,
                AudioRIDurationSec = AudioRIDurationSec,
                ScreenshotsLivetimeDays = ScreenshotsLivetimeDays,
                ScreenshotsStorageMB = ScreenshotsStorageMB
            };
        }

        public override void Update(CommonLocal model)
        {
            Access = model.Access;
            Language = model.Language;
            AWS = model.AWS;
            FolderImagesDrones = model.FolderImagesDrones;
            AudioFileRI = model.AudioFileRI;
            AudioFileJ = model.AudioFileJ;
            FolderRI_UAV = model.FolderRI_UAV;
            CoordinateView = model.CoordinateView;
            IsVisibleEN = model.IsVisibleEN;
            IsVisibleRU = model.IsVisibleRU;
            IsVisibleAZ = model.IsVisibleAZ;
            IsVisibleSR = model.IsVisibleSR;
            AudioFileRadar = model.AudioFileRadar;
            AudioRIDurationSec = model.AudioRIDurationSec;
            ScreenshotsLivetimeDays = model.ScreenshotsLivetimeDays;
            ScreenshotsStorageMB = model.ScreenshotsStorageMB;
        }

        #endregion

        private AccessTypes access = AccessTypes.User;
        private Languages language;
        private byte aws;
        private ViewCoord coordinateView = ViewCoord.Dd;
        private string folderImagesDrones = "";
        private string audioFileRI = "";
        private string audioFileJ = "";
        private string folderRI_UAV = "";
        private int screenshotsLivetimeDays = 1;
        private int screenshotsStorageMB = 1024;
        private int audioRIDurationSec = 0;
        //private int audioRJDurationSec = 0;
        private string audioFileRadar = "";

        private bool en = true;
        private bool ru = true;
        private bool az = true;
        private bool sr = true;

        public AccessTypes Access
        {
            get => access;
            set
            {
                if (access == value) return;
                access = value;
                Console.WriteLine($"Accsess: {access}");
                OnPropertyChanged();
            }
        }

        public Languages Language
        {
            get => language;
            set
            {
                if (language == value) return;
                language = value;
                OnPropertyChanged();
            }
        }

        public byte AWS
        {
            get => aws;
            set
            {
                if (value == aws) return;
                aws = value;
                OnPropertyChanged();
            }
        }

        public string FolderImagesDrones
        {
            get => folderImagesDrones;
            set
            {
                if (folderImagesDrones == value) return;
                folderImagesDrones = value;
                OnPropertyChanged();
            }
        }

        public string AudioFileRI
        {
            get => audioFileRI;
            set
            {
                if (audioFileRI == value) return;
                audioFileRI = value;
                OnPropertyChanged();
            }
        }

        public string AudioFileJ
        {
            get => audioFileJ;
            set
            {
                if (audioFileJ == value) return;
                audioFileJ = value;
                OnPropertyChanged();
            }
        }

        public string FolderRI_UAV
        {
            get => folderRI_UAV;
            set
            {
                if (folderRI_UAV == value) return;
                folderRI_UAV = value;
                OnPropertyChanged();
            }
        }

        public ViewCoord CoordinateView 
        {
            get => coordinateView;
            set
            {
                //if (value == coordinateView) return;             
                coordinateView = value;
                OnPropertyChanged();
            }
        }

        public bool IsVisibleEN
        {
            get => en;
            set
            {             
                if (en == value) return;
                en = value;                
                OnPropertyChanged();
            }
        }

        public bool IsVisibleRU
        {
            get => ru;
            set
            {
                if (ru == value) return;
                ru = value;
                OnPropertyChanged();
            }
        }

        public bool IsVisibleAZ
        {
            get => az;
            set
            {
                if (az == value) return;
                az = value;               
                OnPropertyChanged();
            }
        }

        public bool IsVisibleSR
        {
            get => sr;
            set
            {
                if (sr == value) return;
                sr = value;
                OnPropertyChanged();
            }
        }

        public string AudioFileRadar
        {
            get => this.audioFileRadar;
            set
            {
                if (audioFileRadar == value) return;
                audioFileRadar = value;
                OnPropertyChanged();
            }
        }

        [Range(0, 2147483646)]
        public int AudioRIDurationSec
        {
            get => audioRIDurationSec;
            set
            {
                if (audioRIDurationSec == value) return;
                audioRIDurationSec = value;
                OnPropertyChanged();
            }
        }

        [Range(0, 60)]
        public int ScreenshotsLivetimeDays
        {
            get => screenshotsLivetimeDays;
            set
            {
                if (screenshotsLivetimeDays == value) return;
                screenshotsLivetimeDays = value;
                OnPropertyChanged();
            }
        }

        [Range(0, 102400)]
        public int ScreenshotsStorageMB
        {
            get => screenshotsStorageMB;
            set
            {
                if (screenshotsStorageMB == value) return;
                screenshotsStorageMB = value;
                OnPropertyChanged();
            }
        }

        //[Range(0, 2147483646)]
        //public int AudioJDurationSec
        //{
        //    get => audioRJDurationSec;
        //    set
        //    {
        //        if (audioRJDurationSec == value) return;
        //        audioRJDurationSec = value;
        //        OnPropertyChanged();
        //    }
        //}
    }
}
