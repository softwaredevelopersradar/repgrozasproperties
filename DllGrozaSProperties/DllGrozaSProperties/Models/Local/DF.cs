﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace DllGrozaSProperties.Models.Local
{
   public class DF : AbstractBaseProperties<DF>
    {

        #region Model Methods

        public override bool EqualTo(DF model)
        {
            return IpAddress == model.IpAddress
                && Port == model.Port
                && Type == model.Type
                && ErrorDeg == model.ErrorDeg;
        }

        public override DF Clone()
        {
            return new DF
            {
                IpAddress = IpAddress,
                Port = Port,
                Type = Type,
                ErrorDeg = ErrorDeg
            };
        }

        public override void Update(DF model)
        {
            IpAddress = model.IpAddress;
            Port = model.Port;
            Type = model.Type;
            ErrorDeg = model.ErrorDeg;
        }

        #endregion

        private string ipAddress = "127.0.0.1";
        private int port = 80;
        private TypeDF type = TypeDF.ZhSV;
        private float errorDeg = 0;

        [NotifyParentProperty(true)]
        public TypeDF Type
        {
            get => type;
            set
            {
                if (value == type) return;
                type = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [RegularExpression("\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b", ErrorMessage = "IP : XXX.XXX.XXX.XXX")]
        public string IpAddress
        {
            get => ipAddress;
            set
            {
                if (ipAddress == value) return;
                ipAddress = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [Range(1, 1000000)]
        public int Port
        {
            get => port;
            set
            {
                if (port == value) return;
                port = value;
                OnPropertyChanged();
            }
        }

        [Range(0, 359)]
        public float ErrorDeg
        {
            get => errorDeg;
            set
            {
                if (errorDeg == value) return;
                errorDeg = value;
                OnPropertyChanged();
            }
        }
    }
}
