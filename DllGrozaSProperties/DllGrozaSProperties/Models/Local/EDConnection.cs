﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DllGrozaSProperties.Models.Local
{
    public class EDConnection : AbstractBaseProperties<EDConnection>
    {
        #region IModelMethods

        public override EDConnection Clone()
        {
            return new EDConnection
            {
                ComPort = ComPort,
                Type = Type,
                Existance = Existance,
                IpAddress3G4GRouterLocal = IpAddress3G4GRouterLocal,
                IpAddressCommunicationCenterLocal = IpAddressCommunicationCenterLocal,
                IpAddress3G4GRouterRemote = IpAddress3G4GRouterRemote,
                IpAddressCommunicationCenterRemote = IpAddressCommunicationCenterRemote,
                Port3G4GRouterLocal = Port3G4GRouterLocal,
                PortCommunicationCenterLocal = PortCommunicationCenterLocal,
                Port3G4GRouterRemote = Port3G4GRouterRemote,
                PortCommunicationCenterRemote = PortCommunicationCenterRemote
            };
        }

        public override bool EqualTo(EDConnection model)
        {
            return ComPort == model.ComPort
                && Type == model.Type
                && Existance == model.Existance
                && IpAddress3G4GRouterLocal == model.IpAddress3G4GRouterLocal
                && IpAddressCommunicationCenterLocal == model.IpAddressCommunicationCenterLocal             
                && IpAddress3G4GRouterRemote == model.IpAddress3G4GRouterRemote
                && IpAddressCommunicationCenterRemote == model.IpAddressCommunicationCenterRemote
                && Port3G4GRouterLocal == model.Port3G4GRouterLocal
                && PortCommunicationCenterLocal == model.PortCommunicationCenterLocal
                && Port3G4GRouterRemote == model.Port3G4GRouterRemote
                && PortCommunicationCenterRemote == model.PortCommunicationCenterRemote;                             
        }

        public override void Update(EDConnection model)
        {
            ComPort = model.ComPort;
            Type = model.Type;
            Existance = model.Existance;
            IpAddress3G4GRouterLocal = model.IpAddress3G4GRouterLocal;
            IpAddressCommunicationCenterLocal = model.IpAddressCommunicationCenterLocal;           
            IpAddress3G4GRouterRemote = model.IpAddress3G4GRouterRemote;
            IpAddressCommunicationCenterRemote = model.IpAddressCommunicationCenterRemote;
            Port3G4GRouterLocal = model.Port3G4GRouterLocal;
            PortCommunicationCenterLocal = model.PortCommunicationCenterLocal;
            Port3G4GRouterRemote = model.Port3G4GRouterRemote;
            PortCommunicationCenterRemote = model.PortCommunicationCenterRemote;
        }

        #endregion 

        private string comPort = " ";
        private TypeCnt type = TypeCnt.Motorola;
        private bool existance;

        private string ipAddress3G4GRouterLocal = "127.0.0.1";
        private string ipAddressCommunicationCenterLocal = "127.0.0.1";
        private string ipAddress3G4GRouterRemote = "127.0.0.1";
        private string ipAddressCommunicationCenterRemote = "127.0.0.1";

        private int port3G4GRouterLocal = 80;
        private int portCommunicationCenterLocal = 80;
        private int port3G4GRouterRemote = 80;
        private int portCommunicationCenterRemote = 80;
      

        public string ComPort
        {
            get => comPort;
            set
            {
                if (value == comPort) return;
                comPort = value;
                OnPropertyChanged();
            }
        }

       

        public TypeCnt Type
        {
            get => type;
            set
            {
                if (value == type) return;
                type = value;
                OnPropertyChanged();
            }
        }


        [NotifyParentProperty(true)]
        [Required]
        [RegularExpression("\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b", ErrorMessage = "IP : XXX.XXX.XXX.XXX")]
        public string IpAddress3G4GRouterLocal
        {
            get => ipAddress3G4GRouterLocal;
            set
            {
                if (ipAddress3G4GRouterLocal == value) return;
                ipAddress3G4GRouterLocal = value;
                OnPropertyChanged();
            }
        }


        [NotifyParentProperty(true)]
        [Range(1, 1000000)]
        public int Port3G4GRouterLocal
        {
            get => port3G4GRouterLocal;
            set
            {
                if (port3G4GRouterLocal == value) return;
                port3G4GRouterLocal = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [Required]
        [RegularExpression("\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b", ErrorMessage = "IP : XXX.XXX.XXX.XXX")]
        public string IpAddress3G4GRouterRemote
        {
            get => ipAddress3G4GRouterRemote;
            set
            {
                if (ipAddress3G4GRouterRemote == value) return;
                ipAddress3G4GRouterRemote = value;
                OnPropertyChanged();
            }
        }


        [NotifyParentProperty(true)]
        [Range(1, 1000000)]
        public int Port3G4GRouterRemote
        {
            get => port3G4GRouterRemote;
            set
            {
                if (port3G4GRouterRemote == value) return;
                port3G4GRouterRemote = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [Required]
        [RegularExpression("\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b", ErrorMessage = "IP : XXX.XXX.XXX.XXX")]
        public string IpAddressCommunicationCenterLocal
        {
            get => ipAddressCommunicationCenterLocal;
            set
            {
                if (ipAddressCommunicationCenterLocal == value) return;
                ipAddressCommunicationCenterLocal = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [Range(1, 1000000)]
        public int PortCommunicationCenterLocal
        {
            get => portCommunicationCenterLocal;
            set
            {
                if (portCommunicationCenterLocal == value) return;
                portCommunicationCenterLocal = value;
                OnPropertyChanged();
            }
        }


        [NotifyParentProperty(true)]
        [Required]
        [RegularExpression("\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b", ErrorMessage = "IP : XXX.XXX.XXX.XXX")]
        public string IpAddressCommunicationCenterRemote
        {
            get => ipAddressCommunicationCenterRemote;
            set
            {
                if (ipAddressCommunicationCenterRemote == value) return;
                ipAddressCommunicationCenterRemote = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [Range(1, 1000000)]
        public int PortCommunicationCenterRemote
        {
            get => portCommunicationCenterRemote;
            set
            {
                if (portCommunicationCenterRemote == value) return;
                portCommunicationCenterRemote = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public bool Existance
        {
            get => existance;
            set
            {
                if (value == existance) return;
                existance = value;
                OnPropertyChanged();
            }
        }
    }
}


