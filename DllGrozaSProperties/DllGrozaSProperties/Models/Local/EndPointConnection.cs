﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace DllGrozaSProperties.Models
{
    public class EndPointConnection : AbstractBaseProperties<EndPointConnection>
    {
        #region Model Methods

        public override bool EqualTo(EndPointConnection model)
        {
            return IpAddress == model.IpAddress
                && Port == model.Port;
        }

        public override EndPointConnection Clone()
        {
            return new EndPointConnection
            {
                IpAddress = IpAddress,
                Port = Port
            };
        }

        public override void Update(EndPointConnection model)
        {
            IpAddress = model.IpAddress;
            Port = model.Port;
        }

        #endregion

        private string ipAddress = "127.0.0.1";
        private int port = 80;

        [NotifyParentProperty(true)]
        [Required]
        [RegularExpression("\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b", ErrorMessage = "IP : XXX.XXX.XXX.XXX")]
        public string IpAddress
        {
            get => ipAddress;
            set
            {
                if (ipAddress == value) return;
                ipAddress = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [Range(1, 1000000)]
        public int Port
        {
            get => port;
            set
            {
                if (port == value) return;
                port = value;
                OnPropertyChanged();
            }
        }
    }
}
