﻿using System.ComponentModel;

namespace DllGrozaSProperties.Models
{
    public enum Languages : byte
    {
        [Description("Русский")]
        RU,
        [Description("English")]
        EN,
        [Description("Azərbaycan")]
        AZ,
        [Description("Српски")]
        SR
    }

    public enum TypeDF : byte
    {
        ZhSV = 1,
        ShIV = 2,
        KiAG = 3
    }

    public enum TypeBRD : byte
    {
        [Description("2TS")]
	    _2TS = 0,
        [Description("Impressa")]
        Impressa = 1
    }


    public enum AccessTypes : byte
    {
        Admin,
        User
    }

    public enum Projections: byte
    {
        Geo = 1,
        Mercator = 2
    }

    public enum TypeCnt : byte
    {
        Motorola = 1,
        Cisco = 2,
        Router3G_4G = 3
    }

    public enum ViewCoord : byte
    {
        [Description("DD.dddddd")]
        Dd = 1,
        [Description("DD MM.mmmm")]
        DMm = 2,
        [Description("DD MM SS.ss")]
        DMSs = 3
    }

    public enum SCANCH : byte
    {
        [Description("1")]
        One = 1,
        [Description("2")]
        Two = 2,
        [Description("1+2")]
        OnePlusTwo = 3,
        [Description("12")]
        OneTwo = 4
    }

    public enum TypeSpoofing : byte
    {
        [Description("1")]
        One = 0,
        [Description("2")]
        Two = 1,
        [Description("4")]
        Four = 2
    }
}
