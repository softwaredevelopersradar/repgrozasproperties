﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace DllGrozaSProperties.Models.Local
{
    public class GNSS : AbstractBaseProperties<GNSS>
    {
        #region ModelMethods

        public override bool EqualTo(GNSS model)
        {
            return AutoRequestAlmanac == model.AutoRequestAlmanac
                && AutoRequestCoordinate == model.AutoRequestCoordinate
                && AutoRequestTime == model.AutoRequestTime
                && AutoRequestEphemeris == model.AutoRequestEphemeris
                && Existance == model.Existance
                && FileAlmanac == model.FileAlmanac
                && FileEphemeris == model.FileEphemeris
                && IntervalRequestAlmanac == model.IntervalRequestAlmanac
                && IntervalRequestCoordinate == model.IntervalRequestCoordinate
                && IntervalRequestTime == model.IntervalRequestTime
                && IntervalRequestEphemeris == model.IntervalRequestEphemeris
                && IpAddress == model.IpAddress
                && Port == model.Port;
        }

        public override GNSS Clone()
        {
            return new GNSS
            {
                Port = Port,
                IpAddress = IpAddress,
                IntervalRequestEphemeris = IntervalRequestEphemeris,
                IntervalRequestCoordinate = IntervalRequestCoordinate,
                IntervalRequestTime = IntervalRequestTime,
                IntervalRequestAlmanac = IntervalRequestAlmanac,
                FileEphemeris = FileEphemeris,
                FileAlmanac = FileAlmanac,
                Existance = Existance,
                AutoRequestEphemeris = AutoRequestEphemeris,
                AutoRequestCoordinate = AutoRequestCoordinate,
                AutoRequestTime = AutoRequestTime,
                AutoRequestAlmanac = AutoRequestAlmanac
            };

        }

        public override void Update(GNSS model)
        {
            AutoRequestAlmanac = model.AutoRequestAlmanac;
            AutoRequestCoordinate = model.AutoRequestCoordinate;
            AutoRequestTime = model.AutoRequestTime;
            AutoRequestEphemeris = model.AutoRequestEphemeris;
            Existance = model.Existance;
            FileAlmanac = model.FileAlmanac;
            FileEphemeris = model.FileEphemeris;
            IntervalRequestAlmanac = model.IntervalRequestAlmanac;
            IntervalRequestCoordinate = model.IntervalRequestCoordinate;
            IntervalRequestTime = model.IntervalRequestTime;
            IntervalRequestEphemeris = model.IntervalRequestEphemeris;
            IpAddress = model.IpAddress;
            Port = model.Port;
        }

        #endregion

        private bool existance;
        private string ipAddress = "127.0.0.1";
        private int port = 88;
        private bool autoRequestCoordinate;
        private short intervalRequestCoordinate;        
        private bool autoRequestTime;
        private short intervalRequestTime;
        private bool autoRequestEphemeris;
        private short intervalRequestEphemeris;
        private bool autoRequestAlmanac;
        private short intervalRequestAlmanac;
        private string fileEphemeris = "";
        private string fileAlmanac = "";

        [NotifyParentProperty(true)]
        public bool Existance
        {
            get => existance;
            set
            {
                if (value == existance) return;
                existance = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [RegularExpression("\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b", ErrorMessage = "IP : XXX.XXX.XXX.XXX")]
        public string IpAddress
        {
            get => ipAddress;
            set
            {
                if (ipAddress == value) return;
                ipAddress = value;
                OnPropertyChanged();
            }
        }


        [NotifyParentProperty(true)]
        [Range(1, 1000000)]
        public int Port
        {
            get => port;
            set
            {
                if (port == value) return;
                port = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public bool AutoRequestCoordinate
        {
            get => autoRequestCoordinate;
            set
            {
                if (autoRequestCoordinate == value) return;
                autoRequestCoordinate = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public short IntervalRequestCoordinate
        {
            get => intervalRequestCoordinate;
            set
            {
                if (intervalRequestCoordinate == value) return;
                intervalRequestCoordinate = value;
                OnPropertyChanged();

            }
        }


        [NotifyParentProperty(true)]
        public bool AutoRequestTime
        {
            get => autoRequestTime;
            set
            {
                if (autoRequestTime == value) return;
                autoRequestTime = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public short IntervalRequestTime
        {
            get => intervalRequestTime;
            set
            {
                if (intervalRequestTime == value) return;
                intervalRequestTime = value;
                OnPropertyChanged();

            }
        }



        [NotifyParentProperty(true)]
        public bool AutoRequestEphemeris
        {
            get => autoRequestEphemeris;
            set
            {
                if (autoRequestEphemeris == value) return;
                autoRequestEphemeris = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public short IntervalRequestEphemeris
        {
            get => intervalRequestEphemeris;
            set
            {
                if (intervalRequestEphemeris == value) return;
                intervalRequestEphemeris = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public bool AutoRequestAlmanac
        {
            get => autoRequestAlmanac;
            set
            {
                if (autoRequestAlmanac == value) return;
                autoRequestAlmanac = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public short IntervalRequestAlmanac
        {
            get => intervalRequestAlmanac;
            set
            {
                if (intervalRequestAlmanac == value) return;
                intervalRequestAlmanac = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public string FileEphemeris
        {
            get => fileEphemeris;
            set
            {
                if (fileEphemeris == value) return;
                fileEphemeris = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public string FileAlmanac
        {
            get => fileAlmanac;
            set
            {
                if (fileAlmanac == value) return;
                fileAlmanac = value;
                OnPropertyChanged();
            }
        }
    }
}
