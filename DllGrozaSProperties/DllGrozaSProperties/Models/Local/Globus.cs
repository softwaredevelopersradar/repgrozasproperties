﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace DllGrozaSProperties.Models.Local
{
    public class Globus : AbstractBaseProperties<Globus>
    {       
        private bool existance;
        private const string categoryLocal = "LocalPoint";
        private const string categoryRemoute = "RemotePoint";
        private string ipAddressLocal = "127.0.0.1";
        private int portLocal = 80;
        private string ipAddressRemoute = "127.0.0.1";
        private int portRemoute = 80;
        private double _latitude = -1;
        private double _longitude = -1;
        private double height = -1;
        private double magHeading = -1;
        private short _lifeTime = 30;


        [NotifyParentProperty(true)]
        public bool Existance
        {
            get => existance;
            set
            {
                if (value == existance) return;
                existance = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [Category(categoryLocal)]
        [Required]
        [RegularExpression("\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b", ErrorMessage = "IP : XXX.XXX.XXX.XXX")]
        public string IpAddressLocal
        {
            get => ipAddressLocal;
            set
            {
                if (ipAddressLocal == value) return;
                ipAddressLocal = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [Category(categoryLocal)]
        [Range(1, 1000000)]
        public int PortLocal
        {
            get => portLocal;
            set
            {
                if (portLocal == value) return;
                portLocal = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [Category(categoryRemoute)]
        [Required]
        [RegularExpression("\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b", ErrorMessage = "IP : XXX.XXX.XXX.XXX")]
        public string IpAddressRemoute
        {
            get => ipAddressRemoute;
            set
            {
                if (ipAddressRemoute == value) return;
                ipAddressRemoute = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [Category(categoryRemoute)]
        [Range(1, 1000000)]
        public int PortRemoute
        {
            get => portRemoute;
            set
            {
                if (portRemoute == value) return;
                portRemoute = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public double Height
        {
            get => height;
            set
            {
                if (height == value) return;
                height = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public double MagHeading
        {
            get => magHeading;
            set
            {
                if (magHeading == value) return;
                magHeading = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public double Latitude
        {
            get => _latitude;
            set
            {
                if (_latitude == value) return;
                _latitude = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public double Longitude
        {
            get => _longitude;
            set
            {
                if (_longitude == value) return;
                _longitude = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public short LifeTime
        {
            get => _lifeTime;
            set
            {
                if (_lifeTime == value) return;
                _lifeTime = value;
                OnPropertyChanged();
            }
        }


        #region IModelMethods

        public override Globus Clone()
        {
            return new Globus
            {
                IpAddressLocal = IpAddressLocal,
                IpAddressRemoute = IpAddressRemoute,
                PortLocal = PortLocal,
                PortRemoute = PortRemoute,
                Existance = Existance,
                Latitude = Latitude,
                Longitude = Longitude,
                LifeTime = LifeTime,
                Height = Height,
                MagHeading = MagHeading,
            };
        }

        public override bool EqualTo(Globus model)
        {
            return IpAddressRemoute == model.IpAddressRemoute
                && IpAddressLocal == model.IpAddressLocal
                && PortRemoute == model.PortRemoute
                && PortLocal == model.PortLocal
                && Existance == model.Existance
                && Latitude == model.Latitude
                && Longitude == model.Longitude
                && LifeTime == model.LifeTime
                && Height == model.Height
                && MagHeading == model.MagHeading;
        }

        public override void Update(Globus model)
        {
            IpAddressLocal = model.IpAddressLocal;
            IpAddressRemoute = model.IpAddressRemoute;
            PortLocal = model.PortLocal;
            PortRemoute = model.PortRemoute;
            Existance = model.Existance;
            Longitude = model.Longitude;
            Latitude = model.Latitude;
            LifeTime = model.LifeTime;
            Height = model.Height;
            MagHeading = model.MagHeading; 
        }

        #endregion
    }
}
