﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace DllGrozaSProperties.Models.Local
{
    public class GrozaR : AbstractBaseProperties<GrozaR>
    {
        private bool _existance;
        private string _comPort = " ";
        private string _hostURL = "";

        [NotifyParentProperty(true)]
        public bool Existance
        {
            get => _existance;
            set
            {
                if (value == _existance) return;
                _existance = value;
                OnPropertyChanged();
            }
        }


        [NotifyParentProperty(true)]
        public string ComPort
        {
            get => _comPort;
            set
            {
                if (value == _comPort) return;
                _comPort = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public string HostURL
        {
            get => _hostURL;
            set
            {
                if (_hostURL == value) return;
                _hostURL = value;
                OnPropertyChanged();
            }
        }

        #region IModelMethods

        public override GrozaR Clone()
        {
            return new GrozaR
            {
                Existance = Existance,
                ComPort = ComPort,
                HostURL = HostURL,
               
            };
        }

        public override bool EqualTo(GrozaR model)
        {
            return Existance == model.Existance                
                && ComPort == model.ComPort
                && HostURL == model.HostURL;
        }

        public override void Update(GrozaR model)
        {
            Existance = model.Existance;
            ComPort = model.ComPort;
            HostURL = model.HostURL;            
        }

        #endregion

    }
}
