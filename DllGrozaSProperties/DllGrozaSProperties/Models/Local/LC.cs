﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DllGrozaSProperties.Models.Local
{
    public class LC : AbstractBaseProperties<LC>
    {
        #region Model Methods

        public override bool EqualTo(LC model)
        {
            return IpAddress == model.IpAddress
                   && Port == model.Port
                   && Existance == model.Existance;
        }

        public override LC Clone()
        {
            return new LC
            {
                IpAddress = IpAddress,
                Port = Port,
                Existance = Existance,
            };
        }

        public override void Update(LC model)
        {
            IpAddress = model.IpAddress;
            Port = model.Port;
            Existance = model.Existance;
        }

        #endregion

        private string ipAddress = "127.0.0.1";
        private int port = 80;
        private bool existance;

        [NotifyParentProperty(true)]
        [Required]
        [RegularExpression("\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b", ErrorMessage = "IP : XXX.XXX.XXX.XXX")]
        public string IpAddress
        {
            get => ipAddress;
            set
            {
                if (ipAddress == value) return;
                ipAddress = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [Range(1, 1000000)]
        public int Port
        {
            get => port;
            set
            {
                if (port == value) return;
                port = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public bool Existance
        {
            get => existance;
            set
            {
                if (value == existance) return;
                existance = value;
                OnPropertyChanged();
            }
        }

    }
}
