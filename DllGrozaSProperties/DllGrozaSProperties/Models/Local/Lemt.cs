﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace DllGrozaSProperties.Models
{
    public class Lemt : AbstractBaseProperties<Lemt>
    {
        #region IModelMethods

        public override Lemt Clone()
        {
            return new Lemt
            {
                IpAddressLocal = IpAddressLocal,
                IpAddressRemoute = IpAddressRemoute,
                PortLocal = PortLocal,
                PortRemoute = PortRemoute,
                AddRemoteIPAddress = AddRemoteIPAddress,
                AddRemotePort = AddRemotePort,
                Human = Human,
                Car = Car,
                Helicopter = Helicopter,
                Group = Group,
                UAV = UAV,
                Tank = Tank,
                IFV = IFV,
                Unknown = Unknown,
                Existance = Existance,
                Latitude = Latitude,
                Longitude = Longitude,
                LifeTime = LifeTime               
            };
        }

        public override bool EqualTo(Lemt model)
        {
            return IpAddressRemoute == model.IpAddressRemoute
                && IpAddressLocal == model.IpAddressLocal
                && PortRemoute == model.PortRemoute
                && PortLocal == model.PortLocal
                && AddRemoteIPAddress == model.AddRemoteIPAddress
                && AddRemotePort == model.AddRemotePort
                && Human == model.Human
                && Car == model.Car
                && Helicopter == model.Helicopter
                && Group == model.Group
                && UAV == model.UAV
                && Tank == model.Tank
                && IFV == model.IFV
                && Unknown == model.Unknown
                && Existance == model.Existance
                && Latitude == model.Latitude
                && Longitude == model.Longitude
                && LifeTime == model.LifeTime;
        }

        public override void Update(Lemt model)
        {
            IpAddressLocal = model.IpAddressLocal;
            IpAddressRemoute = model.IpAddressRemoute;
            PortLocal = model.PortLocal;
            PortRemoute = model.PortRemoute;
            AddRemoteIPAddress = model.AddRemoteIPAddress;
            AddRemotePort = model.AddRemotePort;
            Human = model.Human;
            Car = model.Car;
            Helicopter = model.Helicopter;
            Group = model.Group;
            UAV = model.UAV;
            Tank = model.Tank;
            IFV = model.IFV;
            Unknown = model.Unknown;
            Existance = model.Existance;
            Longitude = model.Longitude;
            Latitude = model.Latitude;
            LifeTime = model.LifeTime;
        }

        #endregion

        private bool human;
        private bool car;
        private bool helicopter;
        private bool group;
        private bool uav;
        private bool tank;
        private bool ifv;
        private bool unknown;
        private bool existance;
        private double _latitude = -1;
        private double _longitude = -1;
        private short _lifeTime = 30;

        private const string categoryLocal = "LocalPoint";
        private const string categoryRemoute = "RemotePoint";
        private string ipAddressLocal = "127.0.0.1";
        private int portLocal = 80;
        private string ipAddressRemoute = "127.0.0.1";
        private int portRemoute = 80;
        private string addRemoteIPAddress = "127.0.0.1";
        private int addRemotePort = 80;

        public Lemt()
        {
        }

        [NotifyParentProperty(true)]
        public bool Existance
        {
            get => existance;
            set
            {
                if (value == existance) return;
                existance = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [Category(categoryLocal)]
        [Required]
        [RegularExpression("\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b", ErrorMessage = "IP : XXX.XXX.XXX.XXX")]
        public string IpAddressLocal
        {
            get => ipAddressLocal;
            set
            {
                if (ipAddressLocal == value) return;
                ipAddressLocal = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [Category(categoryLocal)]
        [Range(1, 1000000)]
        public int PortLocal
        {
            get => portLocal;
            set
            {
                if (portLocal == value) return;
                portLocal = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [Category(categoryRemoute)]
        [Required]
        [RegularExpression("\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b", ErrorMessage = "IP : XXX.XXX.XXX.XXX")]
        public string IpAddressRemoute
        {
            get => ipAddressRemoute;
            set
            {
                if (ipAddressRemoute == value) return;
                ipAddressRemoute = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [Category(categoryRemoute)]
        [Range(1, 1000000)]
        public int PortRemoute
        {
            get => portRemoute;
            set
            {
                if (portRemoute == value) return;
                portRemoute = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [Category(categoryRemoute)]
        [Required]
        [RegularExpression("\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b", ErrorMessage = "IP : XXX.XXX.XXX.XXX")]
        public string AddRemoteIPAddress
        {
            get => addRemoteIPAddress;
            set
            {
                if (addRemoteIPAddress == value) return;
                addRemoteIPAddress = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [Category(categoryRemoute)]
        [Range(1, 1000000)]
        public int AddRemotePort
        {
            get => addRemotePort;
            set
            {
                if (addRemotePort == value) return;
                addRemotePort = value;
                OnPropertyChanged();
            }
        }



        [NotifyParentProperty(true)]
        [Category(nameof(Human))]
        public bool Human
        {
            get => human;
            set
            {
                if (human == value) return;
                human = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [Category(nameof(Car))]
        public bool Car
        {
            get => car;
            set
            {
                if (car == value) return;
                car = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [Category(nameof(Helicopter))]
        public bool Helicopter
        {
            get => helicopter;
            set
            {
                if (helicopter == value) return;
                helicopter = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [Category(nameof(Group))]
        public bool Group
        {
            get => group;
            set
            {
                if (group == value) return;
                group = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [Category(nameof(UAV))]
        public bool UAV
        {
            get => uav;
            set
            {
                if (uav == value) return;
                uav = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [Category(nameof(Tank))]
        public bool Tank
        {
            get => tank;
            set
            {
                if (tank == value) return;
                tank = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [Category(nameof(IFV))]
        public bool IFV
        {
            get => ifv;
            set
            {
                if (ifv == value) return;
                ifv = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [Category(nameof(Unknown))]
        public bool Unknown
        {
            get => unknown;
            set
            {
                if (unknown == value) return;
                unknown = value;
                OnPropertyChanged();
            }
        }

        public double Latitude
        {
            get => _latitude;
            set
            {
                if (_latitude == value) return;
                _latitude = value;
                OnPropertyChanged();
            }
        }

        public double Longitude
        {
            get => _longitude;
            set
            {
                if (_longitude == value) return;
                _longitude = value;
                OnPropertyChanged();
            }
        }

        public short LifeTime
        {
            get => _lifeTime;
            set
            {
                if (_lifeTime == value) return;
                _lifeTime = value;
                OnPropertyChanged();
            }
        }
    }
}
