﻿using DllGrozaSProperties.Models.Local;
using System.ComponentModel;
using System.Windows.Controls.WpfPropertyGrid;

namespace DllGrozaSProperties.Models
{
    [CategoryOrder(nameof(General), 1)]
    [CategoryOrder(nameof(DB), 2)]
    [CategoryOrder(nameof(DF), 3)]
    [CategoryOrder(nameof(RS), 4)]
    [CategoryOrder(nameof(SS), 5)]
    [CategoryOrder(nameof(ED), 6)]
    [CategoryOrder(nameof(LC), 7)]
    [CategoryOrder(nameof(PC), 8)]
    [CategoryOrder(nameof(EOM), 9)]
    [CategoryOrder(nameof(Rodnik), 10)]
    [CategoryOrder(nameof(Globus), 11)]
    [CategoryOrder(nameof(Lemt), 12)]
    [CategoryOrder(nameof(BRD1), 13)]
    [CategoryOrder(nameof(BRD2), 14)]
    [CategoryOrder(nameof(CmpRX), 15)]
    [CategoryOrder(nameof(CmpTX), 16)]
    [CategoryOrder(nameof(Map), 17)]
    [CategoryOrder(nameof(Cuirasse_M),18)]
    [CategoryOrder(nameof(Aeroscope), 19)]
    [CategoryOrder(nameof(Amp1), 20)]
    [CategoryOrder(nameof(Amp2), 21)]
    [CategoryOrder(nameof(GNSS), 22)]
    [CategoryOrder(nameof(GrozaR), 23)]

    public class LocalProperties : IModelMethods<LocalProperties>
    {
        public event PropertyChangedEventHandler OnPropertyChanged = (obg, str) => { };
        public LocalProperties()
        {
            General = new CommonLocal();
            DB = new EndPointConnection();
            DF = new DF();
            RS = new RS();
            SS = new SS();
            ED = new EDConnection();
            PC = new PC();
            LC = new LC();
            EOM = new UdpConnection();
            Rodnik = new Rodnik();
            Globus = new Globus();
            Lemt = new Lemt();
            Map = new Map();
            CmpRX = new Cmp();
            CmpTX = new Cmp();
            Amp1 = new Amp();
            Amp2 = new Amp();
            BRD1 = new BRD1();
            BRD2 = new BRD2();
            GNSS = new GNSS();
            Cuirasse_M = new CuirasseM();
            Aeroscope = new Aeroscope();
            PC = new PC();
            GrozaR = new GrozaR();

            General.PropertyChanged += PropertyChanged;
            DB.PropertyChanged += PropertyChanged;
            LC.PropertyChanged += PropertyChanged;
            RS.PropertyChanged += PropertyChanged;
            SS.PropertyChanged += PropertyChanged;
            DF.PropertyChanged += PropertyChanged;
            ED.PropertyChanged += PropertyChanged;
            EOM.PropertyChanged += PropertyChanged;
            Rodnik.PropertyChanged += PropertyChanged;
            Globus.PropertyChanged += PropertyChanged;
            Lemt.PropertyChanged += PropertyChanged;
            Map.PropertyChanged += PropertyChanged;
            CmpRX.PropertyChanged += PropertyChanged;
            CmpTX.PropertyChanged += PropertyChanged;
            Amp1.PropertyChanged += PropertyChanged;
            Amp2.PropertyChanged += PropertyChanged;
            BRD1.PropertyChanged += PropertyChanged;
            BRD2.PropertyChanged += PropertyChanged;
            GNSS.PropertyChanged += PropertyChanged;
            Cuirasse_M.PropertyChanged += PropertyChanged;
            Aeroscope.PropertyChanged += PropertyChanged;
            PC.PropertyChanged += PropertyChanged;
            GrozaR.PropertyChanged += PropertyChanged;


        }

        private void PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
          OnPropertyChanged(this, e);
        }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(General))]
        [DisplayName(" ")]
        public CommonLocal General { get; set; }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(DB))]
        [DisplayName(" ")]
        public EndPointConnection DB { get; set; }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(DF))]
        [DisplayName(" ")]
        public DF DF { get; set; }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(RS))]
        [DisplayName(" ")]
        public RS RS { get; set; }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(SS))]
        [DisplayName(" ")]
        public SS SS { get; set; }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(ED))]
        [DisplayName(" ")]
        public EDConnection ED { get; set; }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(PC))]
        [DisplayName(" ")]
        public PC PC { get; set; }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(LC))]
        [DisplayName(" ")]
        public LC LC { get; set; }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(EOM))]
        [DisplayName(" ")]
        public UdpConnection EOM { get; set; }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(Rodnik))]
        [DisplayName(" ")]
        public Rodnik Rodnik { get; set; }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(Globus))]
        [DisplayName(" ")]
        public Globus Globus { get; set; }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(Lemt))]
        [DisplayName(" ")]
        public Lemt Lemt { get; set; }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(Map))]
        [DisplayName(" ")]
        public Map Map { get; set; }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(CmpRX))]
        [DisplayName(" ")]
        public Cmp CmpRX { get; set; }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(CmpTX))]
        [DisplayName(" ")]
        public Cmp CmpTX { get; set; }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(Cuirasse_M))]
        [DisplayName(" ")]
        public CuirasseM Cuirasse_M { get; set; }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(Aeroscope))]
        [DisplayName(" ")]
        public Aeroscope Aeroscope { get; set; }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(Amp1))]
        [DisplayName(" ")]
        public Amp Amp1 { get; set; }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(Amp2))]
        [DisplayName(" ")]
        public Amp Amp2 { get; set; }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(BRD1))]
        [DisplayName(" ")]
        public BRD1 BRD1 { get; set; }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(BRD2))]
        [DisplayName(" ")]
        public BRD2 BRD2 { get; set; }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(GNSS))]
        [DisplayName(" ")]
        public GNSS GNSS { get; set; }


        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Category(nameof(GrozaR))]
        [DisplayName(" ")]
        public GrozaR GrozaR { get; set; }

        #region Model Methods

        public LocalProperties Clone()
        {
            return new LocalProperties
            {
                General = General.Clone(),
                DB = DB.Clone(),
                DF = DF.Clone(),
                RS = RS.Clone(),
                SS = SS.Clone(),
                ED = ED.Clone(),
                LC = LC.Clone(),
                EOM = EOM.Clone(),
                Rodnik = Rodnik.Clone(),
                Globus = Globus.Clone(),
                Lemt = Lemt.Clone(),
                Map = Map.Clone(),
                CmpRX = CmpRX.Clone(),
                CmpTX = CmpTX.Clone(),
                Amp1 = Amp1.Clone(),
                Amp2 = Amp2.Clone(),
                BRD1 = BRD1.Clone(),
                BRD2 = BRD2.Clone(),
                GNSS = GNSS.Clone(),
                Cuirasse_M = Cuirasse_M.Clone(),
                Aeroscope = Aeroscope.Clone(),
                PC = PC.Clone(),
                GrozaR = GrozaR.Clone(),
            };
        }

        public void Update(LocalProperties localProperties)
        {
            General.Update(localProperties.General);
            DB.Update(localProperties.DB);
            DF.Update(localProperties.DF);
            RS.Update(localProperties.RS);
            SS.Update(localProperties.SS);
            ED.Update(localProperties.ED);
            LC.Update(localProperties.LC);
            EOM.Update(localProperties.EOM);
            Rodnik.Update(localProperties.Rodnik);
            Globus.Update(localProperties.Globus);
            Lemt.Update(localProperties.Lemt);
            Map.Update(localProperties.Map);
            CmpRX.Update(localProperties.CmpRX);
            CmpTX.Update(localProperties.CmpTX);
            GNSS.Update(localProperties.GNSS);
            Amp1.Update(localProperties.Amp1);
            Amp2.Update(localProperties.Amp2);
            Cuirasse_M.Update(localProperties.Cuirasse_M);
            Aeroscope.Update(localProperties.Aeroscope);
            PC.Update(localProperties.PC);
            GrozaR.Update(localProperties.GrozaR);
            ((IModelMethods<BRD1>)BRD1).Update(localProperties.BRD1);
            ((IModelMethods<BRD2>)BRD2).Update(localProperties.BRD2);                
        }

        public bool EqualTo(LocalProperties localProperties)
        {
            return General.EqualTo(localProperties.General)
                && DB.EqualTo(localProperties.DB)
                && DF.EqualTo(localProperties.DF)
                && RS.EqualTo(localProperties.RS)
                && SS.EqualTo(localProperties.SS)
                && ED.EqualTo(localProperties.ED)
                && LC.EqualTo(localProperties.LC)
                && EOM.EqualTo(localProperties.EOM)
                && Rodnik.EqualTo(localProperties.Rodnik)
                && Globus.EqualTo(localProperties.Globus)
                && Lemt.EqualTo(localProperties.Lemt)
                && Map.EqualTo(localProperties.Map)
                && CmpRX.EqualTo(localProperties.CmpRX)
                && CmpTX.EqualTo(localProperties.CmpTX)
                && Amp1.EqualTo(localProperties.Amp1)
                && Amp2.EqualTo(localProperties.Amp2)
                && GNSS.EqualTo(localProperties.GNSS)
                && Cuirasse_M.EqualTo(localProperties.Cuirasse_M)
                && Aeroscope.EqualTo(localProperties.Aeroscope)
                && PC.EqualTo(localProperties.PC)
                && GrozaR.EqualTo(localProperties.GrozaR)
                && ((IModelMethods<BRD1>)BRD1).EqualTo(localProperties.BRD1)
                && ((IModelMethods<BRD2>)BRD2).EqualTo(localProperties.BRD2);
        }
        #endregion
    }
}
