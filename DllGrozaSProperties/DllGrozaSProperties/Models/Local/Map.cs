﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace DllGrozaSProperties.Models.Local
{
    public class Map : AbstractBaseProperties<Map>
    {
        #region IModelMethods

        public override bool EqualTo(Map model)
        {
            return FileMap == model.FileMap
                && FolderMapTiles == model.FolderMapTiles
                && ImageGnssPoint == model.ImageGnssPoint
                && ImageJammer == model.ImageJammer
                && ImageSource == model.ImageSource
                && ImageSpoofingPoint == model.ImageSpoofingPoint
                && ImageCar == model.ImageCar
                && Projection == model.Projection
                && DistanceBearingLine == model.DistanceBearingLine
                && ImageMulticopter == model.ImageMulticopter
                && CurrentScale == model.CurrentScale
                && ImagePlane == model.ImagePlane
                && FileHeightMatrix == model.FileHeightMatrix
                && ZoneJammingRadius == model.ZoneJammingRadius
                && Direction == model.Direction
                && Latitude == model.Latitude
                && Longitude == model.Longitude;
        }

        public override Map Clone()
        {
            return new Map
            {
                FileMap = FileMap,
                FolderMapTiles = FolderMapTiles,
                ImageGnssPoint = ImageGnssPoint,
                ImageJammer = ImageJammer,
                ImageSource = ImageSource,
                ImageSpoofingPoint = ImageSpoofingPoint,
                ImageCar = ImageCar,
                Projection = Projection,
                DistanceBearingLine = DistanceBearingLine,
                ImageMulticopter = ImageMulticopter,
                ImagePlane = ImagePlane,
                CurrentScale = CurrentScale,
                FileHeightMatrix = FileHeightMatrix,
                ZoneJammingRadius = ZoneJammingRadius,
                Direction = Direction,
                Latitude = Latitude,
                Longitude = Longitude
            };
        }

        public override void Update(Map model)
        {
            FileMap = model.FileMap;
            FolderMapTiles = model.FolderMapTiles;
            ImageGnssPoint = model.ImageGnssPoint;
            ImageJammer = model.ImageJammer;
            ImageSource = model.ImageSource;
            ImageSpoofingPoint = model.ImageSpoofingPoint;
            ImageCar = model.ImageCar;
            Projection = model.Projection;
            DistanceBearingLine = model.DistanceBearingLine;
            ImagePlane = model.ImagePlane;
            ImageMulticopter = model.ImageMulticopter;
            CurrentScale = model.CurrentScale;
            FileHeightMatrix = model.FileHeightMatrix;
            ZoneJammingRadius = model.ZoneJammingRadius;
            Direction = model.Direction;
            Latitude = model.Latitude;
            Longitude = model.Longitude;
        }

        #endregion

        private string fileMap = "";
        private string folderMapTiles = "";
        private string fileHeightMatrix = "";
        private string imageSource = "";
        private string imageJammer = "";
        private string imageSpoofingPoint = "";
        private string imageGnssPoint = "";
        private string imageCar = "";
        private string imageMulticopter = "";
        private string imagePlane = "";
        private bool direction;
        private Projections projection = Projections.Geo;
        private int distanceBearingLine = 50000;
        private int zoneJammingRadius = 5000;
        private double currentScale;
        private double latitude = -1;
        private double longitude = -1;

        public Map()
        {
        }

        public string FileMap
        {
            get => fileMap;
            set
            {
                if (fileMap == value) return;
                fileMap = value;
                OnPropertyChanged();

            }
        }

        public string FolderMapTiles
        {
            get => folderMapTiles;
            set
            {
                if (folderMapTiles == value) return;
                folderMapTiles = value;
                OnPropertyChanged();
            }
        }

        public string FileHeightMatrix
        {
            get => fileHeightMatrix;
            set
            {
                if (fileHeightMatrix == value) return;
                fileHeightMatrix = value;
                OnPropertyChanged();
            }
        }

        public string ImageMulticopter 
        {
            get => imageMulticopter;
            set
            {
                if (imageMulticopter == value) return;
                imageMulticopter = value;
                OnPropertyChanged();

            }
        }

        public string ImagePlane 
        {
            get => imagePlane;
            set
            {
                if (imagePlane == value) return;
                imagePlane = value;
                OnPropertyChanged();

            }
        }

        public string ImageSource
        {
            get => imageSource;
            set
            {
                if (imageSource == value) return;
                imageSource = value;
                OnPropertyChanged();
            }
        }

        public string ImageJammer
        {
            get => imageJammer;
            set
            {
                if (imageJammer == value) return;
                imageJammer = value;
                OnPropertyChanged();

            }
        }

        public string ImageSpoofingPoint
        {
            get => imageSpoofingPoint;
            set
            {
                if (imageSpoofingPoint == value) return;
                imageSpoofingPoint = value;
                OnPropertyChanged();

            }
        }

        public string ImageGnssPoint
        {
            get => imageGnssPoint;
            set
            {
                if (imageGnssPoint == value) return;
                imageGnssPoint = value;
                OnPropertyChanged();
            }
        }

        public string ImageCar
        {
            get => imageCar;
            set
            {
                if (imageCar == value) return;
                imageCar = value;
                OnPropertyChanged();
            }
        }

        public bool Direction
        {
            get => direction;
            set
            {
                if (value == direction) return;
                direction = value;
                OnPropertyChanged();
            }
        }

        public Projections Projection
        {
            get => projection;
            set
            {
                if (value == projection) return;
                projection = value;
                OnPropertyChanged();
            }
        }

        [Range(10, 1000000)]
        public int DistanceBearingLine
        {
            get => distanceBearingLine;
            set
            {
                if (distanceBearingLine == value) return;
                distanceBearingLine = value;
                OnPropertyChanged();
            }
        }

        [Range(0, 30000)]
        public int ZoneJammingRadius
        {
            get => zoneJammingRadius;
            set
            {
                if (zoneJammingRadius == value) return;
                zoneJammingRadius = value;
                OnPropertyChanged();
            }
        }

        [Browsable(false)]
        public double CurrentScale
        {
            get => currentScale;
            set
            {
                if (currentScale == value) return;
                currentScale = value;
                OnPropertyChanged();
            }
        }

        [Browsable(false)]
        public double Latitude
        {
            get => latitude;
            set
            {
                if (latitude == value) return;
                latitude = value;
                OnPropertyChanged();
            }
        }

        [Browsable(false)]
        public double Longitude
        {
            get => longitude;
            set
            {
                if (longitude == value) return;
                longitude = value;
                OnPropertyChanged();
            }
        }

    }
}
