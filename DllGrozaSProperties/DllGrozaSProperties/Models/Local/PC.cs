﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DllGrozaSProperties.Models.Local
{
    public class PC : AbstractBaseProperties<PC>
    {
        #region IModelMethods

    public override PC Clone()
    {
        return new PC
        {
            ComPort = ComPort,
            Type = Type,
            IpAddress3G4GRouter = IpAddress3G4GRouter,
            IpAddressCiscoLocal = IpAddressCiscoLocal,
            IpAddressCisco = IpAddressCisco,
            Port3G4GRouter = Port3G4GRouter,
            PortCisco = PortCisco,
            Existance = Existance
        };
    }

    public override bool EqualTo(PC model)
    {
            return ComPort == model.ComPort
                && Type == model.Type
                && IpAddress3G4GRouter == model.IpAddress3G4GRouter
                && IpAddressCiscoLocal == model.IpAddressCiscoLocal
                && IpAddressCisco == model.IpAddressCisco
                && Port3G4GRouter == model.Port3G4GRouter
                && PortCisco == model.PortCisco
                && Existance == model.Existance;
    }

    public override void Update(PC model)
    {
        ComPort = model.ComPort;
        Type = model.Type;
        IpAddress3G4GRouter = model.IpAddress3G4GRouter;
        IpAddressCiscoLocal = model.IpAddressCiscoLocal;
        IpAddressCisco = model.IpAddressCisco;
        Port3G4GRouter = model.Port3G4GRouter;
        PortCisco = model.PortCisco;
        Existance = model.Existance;
    }

    #endregion
    private string comPort = " ";
    private TypeCnt type = TypeCnt.Motorola;
    private bool existance;

    private string ipAddress3G4GRouter = "127.0.0.1";
    private string ipAddressCiscoLocal = "127.0.0.1";
    private string ipAddressCisco = "127.0.0.1";

    private int port3G4GRouter = 80;
    private int portCisco = 80;


    public string ComPort
    {
        get => comPort;
        set
        {
            if (value == comPort) return;
            comPort = value;
            OnPropertyChanged();
        }
    }

    public TypeCnt Type
    {
        get => type;
        set
        {
            if (value == type) return;
            type = value;
            OnPropertyChanged();
        }
    }


    [NotifyParentProperty(true)]
    [Required]
    [RegularExpression("\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b", ErrorMessage = "IP : XXX.XXX.XXX.XXX")]
    public string IpAddress3G4GRouter
    {
        get => ipAddress3G4GRouter;
        set
        {
            if (ipAddress3G4GRouter == value) return;
            ipAddress3G4GRouter = value;
            OnPropertyChanged();
        }
    }


    [NotifyParentProperty(true)]
    [Range(1, 1000000)]
    public int Port3G4GRouter
    {
        get => port3G4GRouter;
        set
        {
            if (port3G4GRouter == value) return;
            port3G4GRouter = value;
            OnPropertyChanged();
        }
    }

    [NotifyParentProperty(true)]
    [Required]
    [RegularExpression("\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b", ErrorMessage = "IP : XXX.XXX.XXX.XXX")]
    public string IpAddressCiscoLocal
    {
        get => ipAddressCiscoLocal;
        set
        {
            if (ipAddressCiscoLocal == value) return;
            ipAddressCiscoLocal = value;
            OnPropertyChanged();
        }
    }

    [NotifyParentProperty(true)]
    [Range(1, 1000000)]
    public int PortCisco
    {
        get => portCisco;
        set
        {
            if (portCisco == value) return;
            portCisco = value;
            OnPropertyChanged();
        }
    }


    [NotifyParentProperty(true)]
    [Required]
    [RegularExpression("\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b", ErrorMessage = "IP : XXX.XXX.XXX.XXX")]
    public string IpAddressCisco
    {
        get => ipAddressCisco;
        set
        {
            if (ipAddressCisco == value) return;
            ipAddressCisco = value;
            OnPropertyChanged();
        }
    }


        public bool Existance
        {
            get => existance;
            set
            {
                if (existance == value) return;
                existance = value;
                OnPropertyChanged();
            }
        }
   }
}
