﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DllGrozaSProperties.Models.Local
{
 public class RS : AbstractBaseProperties<RS>
    {
        #region Model Methods

        public override bool EqualTo(RS model)
        {
            return IpAddress == model.IpAddress
                && Port == model.Port
                && Existance == model.Existance
                && ProtocolsRecognition == model.ProtocolsRecognition
                //&& Channel1Request == model.Channel1Request
                //&& Channel2Request == model.Channel2Request
                && Channel1Interval == model.Channel1Interval;
                //&& Channel2Interval == model.Channel2Interval
        }

        public override RS Clone()
        {
            return new RS
            {
                IpAddress = IpAddress,
                Port = Port,
                Existance = Existance,
                ProtocolsRecognition = ProtocolsRecognition,
                Channel1Interval = Channel1Interval,
                //Channel2Interval = Channel2Interval,
                //Channel1Request = Channel1Request,
                //Channel2Request = Channel2Request
            };
        }

        public override void Update(RS model)
        {
            IpAddress = model.IpAddress;
            Port = model.Port;
            Existance = model.Existance;
            ProtocolsRecognition = model.ProtocolsRecognition;
            Channel1Interval = model.Channel1Interval;
            //Channel2Interval = model.Channel2Interval;
            //Channel1Request = model.Channel1Request;
            //Channel2Request = model.Channel2Request;
        }

        #endregion

        private string ipAddress = "127.0.0.1";
        private int port = 80;
        private bool existance;
        private bool protocolsRecognition;
        //private bool channel1Request;
        //private bool channel2Request;
        private int channel1Interval = 1;
        //private int channel2Interval;


        [NotifyParentProperty(true)]
        [Required]
        [RegularExpression("\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b", ErrorMessage = "IP : XXX.XXX.XXX.XXX")]
        public string IpAddress
        {
            get => ipAddress;
            set
            {
                if (ipAddress == value) return;
                ipAddress = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [Range(1, 1000000)]
        public int Port
        {
            get => port;
            set
            {
                if (port == value) return;
                port = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public bool Existance
        {
            get => existance;
            set
            {
                if (value == existance) return;
                existance = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public bool ProtocolsRecognition
        {
            get => protocolsRecognition;
            set
            {
                if (value == protocolsRecognition) return;
                protocolsRecognition = value;
                OnPropertyChanged();
            }
        }

        //[NotifyParentProperty(true)]
        //public bool Channel1Request
        //{
        //    get => channel1Request;
        //    set
        //    {
        //        if (value == channel1Request) return;
        //        channel1Request = value;
        //        OnPropertyChanged();
        //    }
        //}

        [NotifyParentProperty(true)]
        [Range(1, 25)]
        public int Channel1Interval
        {
            get => channel1Interval;
            set
            {
                if (channel1Interval == value) return;
                channel1Interval = value;
                OnPropertyChanged();
            }
        }

        //[NotifyParentProperty(true)]
        //public bool Channel2Request
        //{
        //    get => channel2Request;
        //    set
        //    {
        //        if (value == channel2Request) return;
        //        channel2Request = value;
        //        OnPropertyChanged();
        //    }
        //}

        //[NotifyParentProperty(true)]
        //[Range(1, 25)]
        //public int Channel2Interval
        //{
        //    get => channel2Interval;
        //    set
        //    {
        //        if (channel2Interval == value) return;
        //        channel2Interval = value;
        //        OnPropertyChanged();
        //    }
        //}
    }
}
