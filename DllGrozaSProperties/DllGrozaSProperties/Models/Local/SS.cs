﻿
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace DllGrozaSProperties.Models.Local
{
    public class SS : AbstractBaseProperties<SS>
    {
        #region Model Methods

        public override bool EqualTo(SS model)
        {
            return IpAddress == model.IpAddress
                && Port == model.Port
                && Existance == model.Existance
                && TwoChannels == model.TwoChannels
                && GLONASS == model.GLONASS
                && ManageByAMP == model.ManageByAMP
                && Type == model.Type;
        }

        public override SS Clone()
        {
            return new SS
            {
                IpAddress = IpAddress,
                Port = Port,
                Existance = Existance,
                TwoChannels = TwoChannels,
                GLONASS = GLONASS,
                ManageByAMP = ManageByAMP,
                Type = Type
            };
        }

        public override void Update(SS model)
        {
            IpAddress = model.IpAddress;
            Port = model.Port;
            Existance = model.Existance;
            TwoChannels = model.TwoChannels;
            GLONASS = model.GLONASS;
            ManageByAMP = model.ManageByAMP;
            Type = model.Type;
        }

        #endregion

        private string ipAddress = "127.0.0.1";
        private int port = 80;
        private bool existance;
        private bool twoChannels;
        private bool gLONASS;
        private bool manageByAMP;
        private TypeSpoofing type;

        [NotifyParentProperty(true)]
        [Required]
        [RegularExpression("\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b", ErrorMessage = "IP : XXX.XXX.XXX.XXX")]
        public string IpAddress
        {
            get => ipAddress;
            set
            {
                if (ipAddress == value) return;
                ipAddress = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [Range(1, 1000000)]
        public int Port
        {
            get => port;
            set
            {
                if (port == value) return;
                port = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public bool Existance
        {
            get => existance;
            set
            {
                if (value == existance) return;
                existance = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public bool TwoChannels
        {
            get => twoChannels;
            set
            {
                if (value == twoChannels) return;
                twoChannels = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public bool GLONASS
        {
            get => gLONASS;
            set
            {
                if (value == gLONASS) return;
                gLONASS = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public bool ManageByAMP
        {
            get => manageByAMP;
            set
            {
                if (value == manageByAMP) return;
                manageByAMP = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        public TypeSpoofing Type
        {
            get => type;
            set
            {
                if (value == type) return;
                type = value;
                OnPropertyChanged();
            }
        }
    }
}
