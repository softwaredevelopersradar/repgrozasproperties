﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace DllGrozaSProperties.Models
{
    public class UdpConnection : AbstractBaseProperties<UdpConnection>
    {
        #region Model methods

        public override bool EqualTo(UdpConnection model)
        {
            return Existance == model.Existance
                && IpAddressLocal == model.IpAddressLocal
                && PortLocal == model.PortLocal
                && IpAddressRemoute == model.IpAddressRemoute
                && PortRemoute == model.PortRemoute
                && ErrorDeg == model.ErrorDeg
                && SurveilanceMode == model.SurveilanceMode;
        }

        public override UdpConnection Clone()
        {
            return new UdpConnection
            {
                Existance = Existance,
                IpAddressLocal = IpAddressLocal,
                PortLocal = PortLocal,
                IpAddressRemoute = IpAddressRemoute,
                PortRemoute = PortRemoute,
                ErrorDeg = ErrorDeg,
                SurveilanceMode = SurveilanceMode
            };
        }

        public override void Update(UdpConnection model)
        {
            Existance = model.Existance;
            IpAddressLocal = model.IpAddressLocal;
            IpAddressRemoute = model.IpAddressRemoute;
            PortLocal = model.PortLocal;
            PortRemoute = model.PortRemoute;
            ErrorDeg = model.ErrorDeg;
            SurveilanceMode = model.SurveilanceMode;
        }
        #endregion

        private const string categoryLocal = "LocalPoint";
        private const string categoryRemoute = "RemotePoint";

        private bool existance;

        private string ipAddressLocal = "127.0.0.1";
        private int portLocal = 80;

        private string ipAddressRemoute = "127.0.0.1";
        private int portRemoute = 80;

        private float _errorDeg = 0;

        private bool _surveilanceMode = false;

        [NotifyParentProperty(true)]
        [Category(categoryLocal)]
        public bool Existance
        {
            get => existance;
            set
            {
                if (existance == value) return;
                existance = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [Category(categoryLocal)]
        [Required]
        [RegularExpression("\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b", ErrorMessage = "IP : XXX.XXX.XXX.XXX")]
        public string IpAddressLocal
        {
            get => ipAddressLocal;
            set
            {
                if (ipAddressLocal == value) return;
                ipAddressLocal = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [Category(categoryLocal)]
        [Range(1, 1000000)]
        public int PortLocal
        {
            get => portLocal;
            set
            {
                if (portLocal == value) return;
                portLocal = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [Category(categoryRemoute)]
        [Required]
        [RegularExpression("\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b", ErrorMessage = "IP : XXX.XXX.XXX.XXX")]
        public string IpAddressRemoute
        {
            get => ipAddressRemoute;
            set
            {
                if (ipAddressRemoute == value) return;
                ipAddressRemoute = value;
                OnPropertyChanged();
            }
        }

        [NotifyParentProperty(true)]
        [Category(categoryRemoute)]
        [Range(1, 1000000)]
        public int PortRemoute
        {
            get => portRemoute;
            set
            {
                if (portRemoute == value) return;
                portRemoute = value;
                OnPropertyChanged();
            }
        }


        [Range(0, 359)]
        public float ErrorDeg
        {
            get => _errorDeg;
            set
            {
                if (_errorDeg == value) return;
                _errorDeg = value;
                OnPropertyChanged();
            }
        }


        public bool SurveilanceMode
        {
            get => _surveilanceMode;
            set
            {
                if (_surveilanceMode == value) return;
                _surveilanceMode = value;
                OnPropertyChanged();
            }
        }
    }
}
