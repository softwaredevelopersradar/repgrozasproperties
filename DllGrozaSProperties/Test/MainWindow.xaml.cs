﻿using DllGrozaSProperties.Models;
using System;
using System.Windows;

namespace Test
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    /// 
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
           // prop.SetPassword("123");
            prop.Local.General.Access = AccessTypes.Admin;
            prop.Local.BRD1.Existance = false;
          //  prop.Local.General.IsVisibleAZ = false;

            //prop.Local.General.CoordinateView = ViewCoord.DMm;
            
            prop.Global.Spoofing.Latitude = 10.777;
            prop.Global.Spoofing.Longitude = 104.33;

            prop.Global.GNSS.Latitude = 89.8999;
            prop.Global.GNSS.Longitude = -4.5565;
        }

        private void Prop_OnLocalDefaultButtonClick(object sender, EventArgs e)
        {
            MessageBox.Show("LocalDefault!");
        }

        private void Prop_OnGlobalDefaultButtonClick(object sender, EventArgs e)
        {
            MessageBox.Show("GlobalDefault!");
        }

        private void Prop_OnLanguageChanged(object sender, DllGrozaSProperties.Models.Languages e)
        {
            MessageBox.Show(e.ToString());
        }

        private void prop_OnLocalPropertiesChanged(object sender, DllGrozaSProperties.Models.LocalProperties e)
        {
            //int f = 6;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
           
            prop.UpdateComPorts();
        }

        private void Prop_OnPasswordChecked(object sender, bool e)
        {
            prop.Global.Spoofing.Latitude = 5.54454;
            prop.Global.Spoofing.Longitude = 120.113;

            prop.Global.GNSS.Latitude = 8.8999;
            prop.Global.GNSS.Longitude = -9.1234;

            if (e)
            {
                MessageBox.Show("Good job!");
            }
            else
            {
                MessageBox.Show("Invalid password!");
            }
        }
    }
}
